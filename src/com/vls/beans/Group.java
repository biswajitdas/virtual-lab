package com.vls.beans;

public class Group {
	int id,supervisor;
	String groupname,projectname,projectdetails;
	
	public Group() {
		super();
		// TODO Auto-generated constructor stub
		id = 0;
	}
	public Group(int id, String groupname) {
		super();
		this.id = id;
		this.groupname = groupname;
	}
	
	public Group(int id, int supervisor, String groupname) {
		super();
		this.id = id;
		this.supervisor = supervisor;
		this.groupname = groupname;
	}	
	
	public Group(int id, int supervisor, String groupname, String projectname, String projectdetails) {
		super();
		this.id = id;
		this.supervisor = supervisor;
		this.groupname = groupname;
		this.projectname = projectname;
		this.projectdetails = projectdetails;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getSupervisor() {
		return supervisor;
	}
	public void setSupervisor(int supervisor) {
		this.supervisor = supervisor;
	}
	public String getGroupname() {
		return groupname;
	}
	public void setGroupname(String groupname) {
		this.groupname = groupname;
	}
	public String getProjectname() {
		return projectname;
	}
	public void setProjectname(String projectname) {
		this.projectname = projectname;
	}
	public String getProjectdetails() {
		return projectdetails;
	}
	public void setProjectdetails(String projectdetails) {
		this.projectdetails = projectdetails;
	}
	
	
}
