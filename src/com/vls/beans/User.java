package com.vls.beans;

public class User {
	int id, batch, gender, type, designation, totalprojects, allowedprojects;
	String name, email, department, username, password, address, skills, status;

	public User() {
		super();
	}

	public User(int id, int batch, int gender, int type, int designation, int totalprojects, int allowedprojects,
			String name, String email, String department, String username, String password, String address,
			String skills, String status) {
		super();
		this.id = id;
		this.batch = batch;
		this.gender = gender;
		this.type = type;
		this.designation = designation;
		this.totalprojects = totalprojects;
		this.allowedprojects = allowedprojects;
		this.name = name;
		this.email = email;
		this.department = department;
		this.username = username;
		this.password = password;
		this.address = address;
		this.skills = skills;
		this.status = status;
	}

	public User(int id, int batch, int gender, int totalprojects, int allowedprojects, String name, String username,
			String email, int type, int designation, String department, String address, String skills, String status) {
		super();
		this.id = id;
		this.batch = batch;
		this.gender = gender;
		this.totalprojects = totalprojects;
		this.allowedprojects = allowedprojects;
		this.username = username;
		this.type = type;
		this.designation = designation;
		this.name = name;
		this.email = email;
		this.address = address;
		this.department = department;
		this.skills = skills;
		this.status = status;
	}
	
	

	public User(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public int getDesignation() {
		return designation;
	}

	public void setDesignation(int designation) {
		this.designation = designation;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBatch() {
		return batch;
	}

	public void setBatch(int batch) {
		this.batch = batch;
	}

	public int getGender() {
		return gender;
	}

	public void setGender(int gender) {
		this.gender = gender;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getTotalprojects() {
		return totalprojects;
	}

	public void setTotalprojects(int totalprojects) {
		this.totalprojects = totalprojects;
	}

	public int getAllowedprojects() {
		return allowedprojects;
	}

	public void setAllowedprojects(int allowedprojects) {
		this.allowedprojects = allowedprojects;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}
