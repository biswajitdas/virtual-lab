package com.vls.beans;

import java.util.Calendar;
import java.util.Date;

public class Message {
	int id, userid, groupid, withteacher;

	long sorting;
	
	String message,name;
	
	Date date;


	public Message() {
		super();
		date = new Date();
		sorting = (long) Calendar.getInstance().getTimeInMillis();
	}
	
	
	public Message(int id, int userid, int groupid, int withteacher, int sorting, String message, Date date) {
		super();
		this.id = id;
		this.userid = userid;
		this.groupid = groupid;
		this.withteacher = withteacher;
		this.sorting = sorting;
		this.message = message;
		this.date = date;
	}


	public Message(int userid, int groupid, String message, String name, Date date) {
		super();
		this.userid = userid;
		this.groupid = groupid;
		this.message = message;
		this.name = name;
		this.date = date;
	}


	
	
	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getUserid() {
		return userid;
	}


	public void setUserid(int userid) {
		this.userid = userid;
	}


	public int getGroupid() {
		return groupid;
	}


	public void setGroupid(int groupid) {
		this.groupid = groupid;
	}


	public int getWithteacher() {
		return withteacher;
	}


	public void setWithteacher(int withteacher) {
		this.withteacher = withteacher;
	}


	public long getSorting() {
		return sorting;
	}


	public void setSorting(long sorting) {
		this.sorting = sorting;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}

	
	public String getDateString() {
		return date.toString();
	}
	
	
	public Date getDate() {
		return date;
	}


	public void setDate(Date date) {
		this.date = date;
	}

	
}
