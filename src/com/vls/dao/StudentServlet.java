package com.vls.dao;

import static com.vls.beans.Provider.USERTABLE;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/student/uploadServlet")
@MultipartConfig(maxFileSize = 16177215) // upload file's size up to 16MB
public class StudentServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// gets values of text fields
		// Calendar calendar = Calendar.getInstance();
		// java.util.Date currentDate = calendar.getTime();
		// java.sql.Date date = new java.sql.Date(currentDate.getTime());

		int id = (Integer.parseInt(request.getParameter("id")) != 0) ? Integer.parseInt(request.getParameter("id"))
				: -1;

		String name = request.getParameter("name");
		String email = request.getParameter("email");
		int batch = Integer.parseInt(request.getParameter("batch"));
		String dept = request.getParameter("department");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		int gender = Integer.parseInt(request.getParameter("gender"));
		String address = request.getParameter("address");
		int type = Integer.parseInt(request.getParameter("type"));
		int designation = Integer.parseInt(request.getParameter("designation"));
		String status = request.getParameter("status");

		InputStream inputStream = null; // input stream of the upload file

		// obtains the upload file part in this multipart request
		Part filePart = request.getPart("image");
		if (filePart != null) {
			// prints out some information for debugging
			System.out.println(filePart.getName());
			System.out.println(filePart.getSize());
			System.out.println(filePart.getContentType());

			// obtains input stream of the upload file
			inputStream = filePart.getInputStream();
		}

		Connection conn = null; // connection to the database
		String message = null; // message will be sent back to client

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = null;

			if (id == -1) {
				ps = con.prepareStatement("INSERT INTO " + USERTABLE
						+ "(name,email,batch,department,username,password,address,gender,type,designation,image) values(?,?,?,?,?,?,?,?,?,?,?)");
				ps.setString(1, name);
				ps.setString(2, email);
				ps.setInt(3, batch);
				ps.setString(4, dept);
				ps.setString(5, username);
				ps.setString(6, password);
				ps.setString(7, address);
				ps.setInt(8, gender);
				ps.setInt(9, type);
				ps.setInt(10, type);
				ps.setBlob(11, inputStream);

				int row = ps.executeUpdate();
				if (row > 0) {
					message = "File uploaded and saved into database";

					getServletContext().getRequestDispatcher("/student/index.jsp?success=1").forward(request, response);

				}

			} else {

				String skills = request.getParameter("skills");

				if (filePart.getSize() != 0) {
					ps = con.prepareStatement("UPDATE " + USERTABLE
							+ " SET name=?,batch=?,email=?,department=?,address=?,status=?,gender=?,type=?,designation=?,skills=?,image=? WHERE id=?");
					ps.setBlob(11, inputStream);
					ps.setInt(12, id);
				} else {
					ps = con.prepareStatement("UPDATE " + USERTABLE
							+ " SET name=?,batch=?,email=?,department=?,address=?,status=?,gender=?,type=?,designation=?,skills=? WHERE id=?");
					ps.setInt(11, id);
				}

				ps.setString(1, name);
				ps.setInt(2, batch);
				ps.setString(3, email);
				ps.setString(4, dept);
				ps.setString(5, address);
				ps.setString(6, status);
				ps.setInt(7, gender);
				ps.setInt(8, type);
				ps.setInt(9, designation);
				ps.setString(10, skills);

				/*
				 * if (inputStream != null) { // fetches input stream of the upload file for the
				 * ps.setBlob(9, inputStream); }
				 */

				// sends the statement to the database server
				int row = ps.executeUpdate();
				if (row > 0) {
					message = "File uploaded and saved into database";
					getServletContext().getRequestDispatcher("/student/editprofile.jsp?success=1").forward(request,
							response);

					// response.sendRedirect("../student/editprofile.jsp?success=1");
				}
			}

			// sends the statement to the database server

		} catch (SQLException ex) {
			message = "ERROR: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			if (conn != null) {
				// closes the database connection
				try {
					conn.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
			// sets the message in request scope
			// request.setAttribute("Message", message);

			// forwards to the message page

		}
	}
}