package com.vls.dao;

import static com.vls.beans.Provider.USERTABLE;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/admin/uploadServlet")
@MultipartConfig(maxFileSize = 16177215) // upload file's size up to 16MB
public class AdminServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// gets values of text fields
		// Calendar calendar = Calendar.getInstance();
		// java.util.Date currentDate = calendar.getTime();
		// java.sql.Date date = new java.sql.Date(currentDate.getTime());

		int id = (Integer.parseInt(request.getParameter("id")) != 0) ? Integer.parseInt(request.getParameter("id"))
				: -1;

		String name = request.getParameter("name");
		String email = request.getParameter("email");
		String dept = request.getParameter("department");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		int gender = Integer.parseInt(request.getParameter("gender"));
		String address = request.getParameter("address");
		int designation = Integer.parseInt(request.getParameter("designation"));
		int type = Integer.parseInt(request.getParameter("type"));
		String status = request.getParameter("status");

		InputStream inputStream = null; // input stream of the upload file

		// obtains the upload file part in this multipart request
		Part filePart = request.getPart("image");
		if (filePart != null) {
			// prints out some information for debugging
			System.out.println(filePart.getName());
			System.out.println(filePart.getSize());
			System.out.println(filePart.getContentType());

			// obtains input stream of the upload file
			inputStream = filePart.getInputStream();
		}

		Connection conn = null; // connection to the database
		String message = null; // message will be sent back to client

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = null;

			if (id == -1) {
				ps = con.prepareStatement("INSERT INTO " + USERTABLE
						+ "(name,email,department,username,password,address,gender,type,designation,status,image) values(?,?,?,?,?,?,?,?,?,?,?)");
				ps.setString(1, name);
				ps.setString(2, email);
				ps.setString(3, dept);
				ps.setString(4, username);
				ps.setString(5, password);
				ps.setString(6, address);
				ps.setInt(7, gender);
				ps.setInt(8, type);
				ps.setInt(9, designation);
				ps.setString(10, status);
				ps.setBlob(11, inputStream);

				int row = ps.executeUpdate();
				if (row > 0) {
					message = "File uploaded and saved into database";

					getServletContext().getRequestDispatcher("/admin/createadmin.jsp?success=1").forward(request, response);

				}

			} else {

				String skills = request.getParameter("skills");

				if (filePart.getSize() != 0) {
					ps = con.prepareStatement("UPDATE " + USERTABLE
							+ " SET name=?,email=?,department=?,address=?,status=?,gender=?,designation=?,skills=?,image=? WHERE id=?");
					ps.setBlob(9, inputStream);
					ps.setInt(10, id);
				} else {
					ps = con.prepareStatement("UPDATE " + USERTABLE
							+ " SET name=?,email=?,department=?,address=?,status=?,gender=?,designation=?,skills=? WHERE id=?");
					ps.setInt(9, id);
				}

				ps.setString(1, name);				
				ps.setString(2, email);
				ps.setString(3, dept);
				ps.setString(4, address);
				ps.setString(5, status);
				ps.setInt(6, gender);
				ps.setInt(7, designation);
				ps.setString(8, skills);


				/*
				 * if (inputStream != null) { // fetches input stream of the upload file for the
				 * ps.setBlob(9, inputStream); }
				 */

				// sends the statement to the database server
				int row = ps.executeUpdate();
				if (row > 0) {
					message = "File uploaded and saved into database";
					getServletContext().getRequestDispatcher("/admin/editprofile.jsp?success=1").forward(request,
							response);

					// response.sendRedirect("../student/editprofile.jsp?success=1");
				}
			}

			// sends the statement to the database server

		} catch (SQLException ex) {
			message = "ERROR: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			if (conn != null) {
				// closes the database connection
				try {
					conn.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
			// sets the message in request scope
			// request.setAttribute("Message", message);

			// forwards to the message page

		}
	}
}