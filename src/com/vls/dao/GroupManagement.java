package com.vls.dao;

import static com.vls.beans.Provider.*;

import com.vls.beans.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class GroupManagement {

	public static int addGroup(String group) {

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("INSERT INTO groups(groupname) values(?)");

			ps.setString(1, group);

			return ps.executeUpdate();

		} catch (Exception e) {
		}

		return 0;
	}

	public static int addGroupWithDetails(Group g) {

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps;
			if (g.getId() != 0) {
				ps = con.prepareStatement("UPDATE groups set groupname=?, projectname=?, projectdetails=? WHERE id=?");
				ps.setInt(4, g.getId());
			}				
			else
				ps = con.prepareStatement("INSERT INTO groups(groupname,projectname,projectdetails) VALUES(?,?,?)");
			
			ps.setString(1, g.getGroupname());
			ps.setString(2, g.getProjectname());
			ps.setString(3, g.getProjectdetails());

			return ps.executeUpdate();
			
		} catch (Exception e) {
		}

		return 0;
	}

	public static int removeGroup(int gid) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("DELETE FROM groups where id = ?");
			ps.setInt(1, gid);

			return ps.executeUpdate();

		} catch (Exception e) {
		}
		return 0;
	}

	public static List<Group> getAllGroups(int flag) {
		List<Group> list = new ArrayList<Group>();

		try {
			Connection con = DBConnection.getConnection();

			PreparedStatement ps;

			if (flag == -1)
				ps = con.prepareStatement("SELECT * FROM groups");
			else if (flag == 0)
				ps = con.prepareStatement("SELECT * FROM groups WHERE supervisor=0");
			else {
				ps = con.prepareStatement("SELECT * FROM groups WHERE id = ?");
				ps.setInt(1, flag);
			}

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				list.add(new Group(rs.getInt("id"), rs.getInt("supervisor"), rs.getString("groupname")));
			}

		} catch (Exception e) {
		}

		return list;
	}

	public static Group getGroup(int gid) {
		Group group = new Group();

		try {

			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from groups where id = ?");
			ps.setInt(1, gid);

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return new Group(rs.getInt("id"), rs.getInt("supervisor"), rs.getString("groupname"),
						rs.getString("projectname"), rs.getString("projectdetails"));
			}

		} catch (Exception e) {
		}

		return group;
	}

	public static List<Group> getUserGroup(int gid) {

		List<Group> list = new ArrayList<Group>();

		try {
			Connection con = DBConnection.getConnection();

			PreparedStatement ps;

			ps = con.prepareStatement(
					"SELECT G.* FROM (groupmembers as GM inner join groups as G on G.id=GM.groupid)  where GM.memberid=?");
			ps.setInt(1, gid);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				list.add(new Group(rs.getInt("id"), rs.getInt("supervisor"), rs.getString("groupname")));
			}

		} catch (Exception e) {
		}

		return list;
	}
	
	
	public static int getUserGroupId(int memberid) {
		
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from groupmembers where memberid = ?");
			ps.setInt(1, memberid);
			
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				return rs.getInt("groupid");
			}
			
		}catch(Exception e) {}
		
		return 0;
	}
	
	
	public static List<Group> getAllGroupOfSupervisor(int sid){
		List<Group> list = new ArrayList<Group>();
		
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM groups WHERE supervisor = ?");
			ps.setInt(1, sid);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				list.add(new Group(rs.getInt("id"),rs.getInt("supervisor"),rs.getString("groupname")));
			}
			
			return list;
		}catch(Exception e) {}
		
		return null;
	}
	
	public static List<Group> getAllGroupOfUser(int id){
		List<Group> list = new ArrayList<Group>();
		
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("select G.id, G.groupname From groups as G inner join groupmembers AS GM on GM.groupid = G.id where GM.memberid = ?");
			ps.setInt(1, id);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				list.add(new Group(rs.getInt("id"),rs.getString("groupname")));
			}
			
			return list;
		}catch(Exception e) {}
		
		return null;
	}
	

	public static int setSupervisor(int groupid, int supervisorid) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE groups set supervisor=? where id = ?");
			ps.setInt(1, supervisorid);
			ps.setInt(2, groupid);

			return ps.executeUpdate();

		} catch (Exception e) {
		}

		return 0;
	}

	public static int addMembers(int gid, int members[]) {
		try {
			Connection con = DBConnection.getConnection();
			String str = "";

			for (int i = 0; i < members.length; i++) {

				str += (i != 0) ? "," : "";

				str += "(" + gid + "," + members[i] + ")";
			}

			PreparedStatement ps = con.prepareStatement("INSERT INTO groupmembers(groupid,memberid) VALUES " + str);

			removeMembers(gid);

			return ps.executeUpdate();

		} catch (Exception e) {
		}

		return 0;
	}

	public static int addMember(int gid, int member) {
		try {
			Connection con = DBConnection.getConnection();

			PreparedStatement ps = con.prepareStatement("INSERT INTO groupmembers(groupid,memberid) values(?,?)");

			ps.setInt(1, gid);
			ps.setInt(2, member);

			return ps.executeUpdate();

		} catch (Exception e) {
		}

		return 0;
	}

	public static int removeMembers(int gid) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("DELETE FROM groupmembers WHERE groupid = ? ");
			ps.setInt(1, gid);
			return ps.executeUpdate();
		} catch (Exception e) {
		}

		return 0;
	}

	public static int getLastGroupId() {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("select * from groups order by id desc limit 1");

			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				return rs.getInt("id");
			}

		} catch (Exception e) {
		}

		return 0;
	}
}
