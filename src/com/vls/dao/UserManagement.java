package com.vls.dao;

import static com.vls.beans.Provider.USERTABLE;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.vls.beans.Group;
import com.vls.beans.User;

public class UserManagement {
	public static int registerUser(User user) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("INSERT INTO " + USERTABLE
					+ "(name,email,batch,department,username,password,address,gender,type) values(?,?,?,?,?,?,?,?,?)");

			ps.setString(1, user.getName());
			ps.setString(2, user.getEmail());
			ps.setInt(3, user.getBatch());
			ps.setString(4, user.getDepartment());
			ps.setString(5, user.getUsername());
			ps.setString(6, user.getPassword());
			ps.setString(7, user.getAddress());
			ps.setInt(8, user.getGender());
			ps.setInt(9, 1);

			/*
			 * String str = "Name : "+user.getName(); str+="\tEmail : "+user.getEmail();
			 * str+="\tBatch : "+user.getBatch();
			 * str+="\tDepartment : "+user.getDepartment();
			 * str+="\tUsername : "+user.getUsername();
			 * str+="\tpassword : "+user.getPassword();
			 * str+="\taddress : "+user.getAddress(); str+="\tgender : "+user.getGender();
			 * 
			 * return str;
			 */

			return ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return 0;
	}

	public static int signin(User u,int type) {
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement(
					"SELECT id FROM " + USERTABLE + " WHERE username=? and password=? and type=? LIMIT 1");
			ps.setString(1, u.getUsername());
			ps.setString(2, u.getPassword());
			ps.setInt(3, type);
			
			ResultSet rs = ps.executeQuery();

			if (rs.next())
				return rs.getInt("id");

		} catch (Exception e) {
		}

		return 0;
	}

	public static User getUser(int id) {
		User user = null;
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM " + USERTABLE + " WHERE id = " + id);
			// ps.setInt(1, id);

			ResultSet rs = ps.executeQuery();

			while (rs.next()) {
				user = new User(rs.getInt("id"), rs.getInt("batch"), rs.getInt("gender"), rs.getInt("totalprojects"), rs.getInt("allowedprojects"),
						rs.getString("name"), rs.getString("username"), rs.getString("email"), rs.getInt("type"), rs.getInt("designation"), rs.getString("department"),
						rs.getString("address"), rs.getString("skills"), rs.getString("status"));
			}

			return user;

		} catch (Exception e) {
		}

		return user;

	}
	
	public static int updatePassword(int id, String oldpass, String newpass) {

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE "+USERTABLE+" SET password=? WHERE id=? and password=?");

			ps.setString(1, newpass);
			ps.setInt(2, id);
			ps.setString(3, oldpass);

			return ps.executeUpdate();

		} catch (Exception e) {
			
		}

		return 0;
	}
	
	public static List<User> getAllUsers(int type){
		List<User> list = new ArrayList<User>();
		
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT id,name FROM users WHERE type=?");
			ps.setInt(1, type);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				list.add(new User(rs.getInt("id"),rs.getString("name")));
			}
			
		}catch(Exception e) {}
		
		return list;
	}
	
	
	
	public static List<User> getUsersNoGroup(){
		List<User> list = new ArrayList<User>();
		
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("select id,name from users where id not in (select memberid from groupmembers) and type =1");
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				list.add(new User(rs.getInt("id"),rs.getString("name")));
			}
			
		}
		catch(Exception e) {}
		
		return list;
		
	}
	
	public static List<User> getUsersInGroup(int gid){
		List<User> list = new ArrayList<User>();
		
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT U.id,U.name FROM users AS U INNER JOIN groupmembers AS GM WHERE U.id = GM.memberid and GM.groupid=?");
			ps.setInt(1, gid);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				list.add(new User(rs.getInt("id"),rs.getString("name")));
			}
			
		}catch(Exception e) {}
		
		
		return list;
	}
}
