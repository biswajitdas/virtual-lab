package com.vls.dao;

import static com.vls.beans.Provider.*;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnection {
	public static Connection getConnection() {
		Connection con = null;
		
		try {
			Class.forName(DRIVER);
			con = DriverManager.getConnection(CONN_URL,USER,PASS);
			
		}catch(Exception e) {
			
		}
		
		return con;
	}
}
