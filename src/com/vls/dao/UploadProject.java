package com.vls.dao;

import static com.vls.beans.Provider.USERTABLE;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Calendar;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/student/uploadProject")
@MultipartConfig(maxFileSize = 16177215) // upload file's size up to 16MB
public class UploadProject extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String saveFile = "";
		int userid = (request.getParameter("userid") != null) ? Integer.parseInt(request.getParameter("userid")) : 0;
		int groupid = (request.getParameter("groupid") != null) ? Integer.parseInt(request.getParameter("groupid")) : 0;
		String comment = request.getParameter("comment");

		
		String contentType = request.getContentType();
		if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
			
		
			DataInputStream in = new DataInputStream(request.getInputStream());
			int formDataLength = request.getContentLength();
			byte dataBytes[] = new byte[formDataLength];
			int byteRead = 0;
			int totalBytesRead = 0;
			while (totalBytesRead < formDataLength) {
				byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
				totalBytesRead += byteRead;
			}
			String file = new String(dataBytes);
			
			saveFile = file.substring(file.indexOf("filename=\"") + 10);		
			
			//saveFile = saveFile.substring(0, saveFile.indexOf("\n"));	
			
			saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));	
			
			
			saveFile = saveFile.substring(0, saveFile.indexOf(".")) + Calendar.getInstance().getTimeInMillis()
					+ saveFile.substring(saveFile.indexOf("."), saveFile.length());

			String filename = saveFile;
			
			System.out.println(filename);

			int lastIndex = contentType.lastIndexOf("=");
			String boundary = contentType.substring(lastIndex + 1, contentType.length());
			int pos;
			pos = file.indexOf("filename=\"");
			pos = file.indexOf("\n", pos) + 1;
			pos = file.indexOf("\n", pos) + 1;
			pos = file.indexOf("\n", pos) + 1;
			int boundaryLocation = file.indexOf(boundary, pos) - 4;

			int startPos = ((file.substring(0, pos)).getBytes()).length;

			int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;
			//saveFile="C:/UploadedFiles/"+saveFile;
			saveFile = "D:/Collection/Software/Eclipse/eclipse/workspace/VirtualLab/WebContent/UploadedFiles/"
					+ saveFile;

			File f = new File(saveFile);
			FileOutputStream fileOut = new FileOutputStream(f);
			fileOut.write(dataBytes, startPos, (endPos - startPos));
			fileOut.flush();
			fileOut.close();

			/* 		try {
						Connection con = DBConnection.getConnection();
						PreparedStatement ps = con.prepareStatement(
								"INSERT INTO uploadedprojects(uploader,groupid, comment, filename) VALUES(?,?,?,?)");
						
						ps.setInt(1, userid);
						ps.setInt(2, groupid);
						ps.setString(3, comment);
						ps.setString(4, filename);
						
						int x = ps.executeUpdate();
						
						if (x != 0) {
							response.sendRedirect("uploadproject.jsp?success=1");
						} else {
							response.sendRedirect("uploadproject.jsp?error=1");
						}
						
					} catch (Exception e) {
					} */
			
		}
		System.out.println(userid);
		System.out.println(groupid);
		System.out.println(comment);


	}
}