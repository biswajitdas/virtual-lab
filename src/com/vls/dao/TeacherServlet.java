package com.vls.dao;

import static com.vls.beans.Provider.USERTABLE;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

@WebServlet("/teacher/uploadServlet")
@MultipartConfig(maxFileSize = 16177215) // upload file's size up to 16MB
public class TeacherServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// gets values of text fields
		// Calendar calendar = Calendar.getInstance();
		// java.util.Date currentDate = calendar.getTime();
		// java.sql.Date date = new java.sql.Date(currentDate.getTime());

		int id = (Integer.parseInt(request.getParameter("id")) != 0) ? Integer.parseInt(request.getParameter("id"))
				: -1;

		String name = request.getParameter("name");
		String email = request.getParameter("email");
		int allowedprojects = Integer.parseInt(request.getParameter("allowedprojects"));
		String dept = request.getParameter("department");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		int gender = Integer.parseInt(request.getParameter("gender"));
		String address = request.getParameter("address");
		int type = Integer.parseInt(request.getParameter("type"));
		int designation = Integer.parseInt(request.getParameter("des"));
		String status = request.getParameter("status");

		InputStream inputStream = null; // input stream of the upload file

		// obtains the upload file part in this multipart request
		Part filePart = request.getPart("image");
		if (filePart != null) {
			// prints out some information for debugging
			System.out.println(filePart.getName());
			System.out.println(filePart.getSize());
			System.out.println(filePart.getContentType());

			// obtains input stream of the upload file
			inputStream = filePart.getInputStream();
		}

		Connection conn = null; // connection to the database
		String message = null; // message will be sent back to client

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = null;

			if (id == -1) {
				ps = con.prepareStatement("INSERT INTO " + USERTABLE
						+ "(name,email,allowedprojects,department,username,password,address,gender,type,designation,status,image) values(?,?,?,?,?,?,?,?,?,?,?,?)");
				ps.setString(1, name);
				ps.setString(2, email);
				ps.setInt(3, allowedprojects);
				ps.setString(4, dept);
				ps.setString(5, username);
				ps.setString(6, password);
				ps.setString(7, address);
				ps.setInt(8, gender);
				ps.setInt(9, type);
				ps.setInt(10, designation);
				ps.setString(11, status);
				ps.setBlob(12, inputStream);

				int row = ps.executeUpdate();
				if (row > 0) {
					message = "File uploaded and saved into database";

					getServletContext().getRequestDispatcher("/teacher/index.jsp?success=1").forward(request, response);

				}

			} else {

				String skills = request.getParameter("skills");

				if (filePart.getSize() != 0) {
					ps = con.prepareStatement("UPDATE " + USERTABLE
							+ " SET name=?,email=?,department=?,address=?,status=?,gender=?,type=?,designation=?,skills=?,allowedprojects=?,image=? WHERE id=?");
					ps.setBlob(11, inputStream);
					ps.setInt(12, id);
				} else {
					ps = con.prepareStatement("UPDATE " + USERTABLE
							+ " SET name=?,email=?,department=?,address=?,status=?,gender=?,type=?,designation=?,skills=?,allowedprojects=? WHERE id=?");
					ps.setInt(11, id);
				}

				ps.setString(1, name);				
				ps.setString(2, email);
				ps.setString(3, dept);
				ps.setString(4, address);
				ps.setString(5, status);
				ps.setInt(6, gender);
				ps.setInt(7, type);
				ps.setInt(8, designation);
				ps.setString(9, skills);
				ps.setInt(10, allowedprojects);

				/*
				 * if (inputStream != null) { // fetches input stream of the upload file for the
				 * ps.setBlob(9, inputStream); }
				 */

				// sends the statement to the database server
				int row = ps.executeUpdate();
				if (row > 0) {
					message = "File uploaded and saved into database";
					getServletContext().getRequestDispatcher("/teacher/editprofile.jsp?success=1").forward(request,
							response);

					// response.sendRedirect("../student/editprofile.jsp?success=1");
				}
			}

			// sends the statement to the database server

		} catch (SQLException ex) {
			message = "ERROR: " + ex.getMessage();
			ex.printStackTrace();
		} finally {
			if (conn != null) {
				// closes the database connection
				try {
					conn.close();
				} catch (SQLException ex) {
					ex.printStackTrace();
				}
			}
			// sets the message in request scope
			// request.setAttribute("Message", message);

			// forwards to the message page

		}
	}
}