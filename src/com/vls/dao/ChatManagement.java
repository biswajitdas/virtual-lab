package com.vls.dao;

import static com.vls.beans.Provider.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.vls.beans.Message;

public class ChatManagement {

	public static int postMessage(Message msg) {
		
		Calendar calendar = Calendar.getInstance();
		java.util.Date currentDate = calendar.getTime();
		java.sql.Date date = new java.sql.Date(currentDate.getTime());

		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con
					.prepareStatement("INSERT INTO chat(userid, groupid, withteacher, message, sorting, date) VALUES(?,?,?,?,?,?)");
			
			ps.setInt(1, msg.getUserid());
			ps.setInt(2, msg.getGroupid());
			ps.setInt(3, msg.getWithteacher());
			ps.setString(4, msg.getMessage());
			ps.setLong(5,msg.getSorting());
			ps.setDate(6, date);
			
			return ps.executeUpdate();	
			
			
		} catch (Exception e) {
		}

		return 0;
	}
	
	public static List<Message> getMessage(int groupid,int withteacher){
		List<Message> list = new ArrayList<Message>();
		
		try {
			Connection con = DBConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT C.*,U.name FROM chat as C inner join users as U on C.userid=U.id WHERE groupid = ? and withteacher=? order by sorting");
			ps.setInt(1, groupid);
			ps.setInt(2, withteacher);
			
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				list.add(new Message(rs.getInt("userid"),rs.getInt("groupid"),rs.getString("message"),rs.getString("name"),rs.getDate("date")));
			}
			
		}catch(Exception e) {}
		
		return list;
	}

}
