<%@page
	import="java.io.*, java.sql.*, java.util.*, com.vls.beans.*,com.vls.dao.*"%>
<%
	String saveFile = "";

	String contentType = request.getContentType();
	if ((contentType != null) && (contentType.indexOf("multipart/form-data") >= 0)) {
		DataInputStream in = new DataInputStream(request.getInputStream());
		int formDataLength = request.getContentLength();
		byte dataBytes[] = new byte[formDataLength];
		int byteRead = 0;
		int totalBytesRead = 0;
		while (totalBytesRead < formDataLength) {
			byteRead = in.read(dataBytes, totalBytesRead, formDataLength);
			totalBytesRead += byteRead;
		}
		String file = new String(dataBytes);

		saveFile = file.substring(file.indexOf("filename=\"") + 10);

		saveFile = saveFile.substring(0, saveFile.indexOf("\n"));

		saveFile = saveFile.substring(saveFile.lastIndexOf("\\") + 1, saveFile.indexOf("\""));

		saveFile = saveFile.substring(0, saveFile.indexOf(".")) + Calendar.getInstance().getTimeInMillis()
				+ saveFile.substring(saveFile.indexOf("."), saveFile.length());

		String filename = saveFile;

		int lastIndex = contentType.lastIndexOf("=");
		String boundary = contentType.substring(lastIndex + 1, contentType.length());
		int pos;
		pos = file.indexOf("filename=\"");
		pos = file.indexOf("\n", pos) + 1;
		pos = file.indexOf("\n", pos) + 1;
		pos = file.indexOf("\n", pos) + 1;
		int boundaryLocation = file.indexOf(boundary, pos) - 4;

		int startPos = ((file.substring(0, pos)).getBytes()).length;

		int endPos = ((file.substring(0, boundaryLocation)).getBytes()).length;
		//saveFile="C:/UploadedFiles/"+saveFile;
		saveFile = "D:/Collection/Software/Eclipse/eclipse/workspace/VirtualLab/WebContent/UploadedFiles/"
				+ saveFile;

		File f = new File(saveFile);
		FileOutputStream fileOut = new FileOutputStream(f);
		fileOut.write(dataBytes, startPos, (endPos - startPos));
		fileOut.flush();
		fileOut.close();

		try {
			Connection con = DBConnection.getConnection();
			
			
			PreparedStatement ps = con.prepareStatement("SELECT * FROM uploadedprojects ORDER BY id DESC limit 1");
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				ps = con.prepareStatement("UPDATE uploadedprojects SET filename=? WHERE id=?");

				ps.setString(1, filename);
				ps.setInt(2, rs.getInt("id"));

				int x = ps.executeUpdate();

				if (x != 0) {
					response.sendRedirect("uploadproject.jsp?success=1");
				} else {
					response.sendRedirect("uploadproject.jsp?error=1");
				}
				
				
			}
			
			

		} catch (Exception e) {
		}

	}
%>

