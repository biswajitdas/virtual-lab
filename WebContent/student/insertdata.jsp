<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*,com.vls.beans.*,com.vls.dao.*,java.sql.*"%>

<%
	int userid = (request.getParameter("userid") != null) ? Integer.parseInt(request.getParameter("userid"))
			: 0;
	int groupid = (request.getParameter("groupid") != null) ? Integer.parseInt(request.getParameter("groupid"))
			: 0;
	String comment = request.getParameter("comment");
	
	
	Calendar calendar = Calendar.getInstance();
	java.util.Date currentDate = calendar.getTime();
	java.sql.Date date = new java.sql.Date(currentDate.getTime());
	
	try{
		Connection con = DBConnection.getConnection();
		PreparedStatement ps = con.prepareStatement("INSERT INTO uploadedprojects(uploader,groupid,comment,date) VALUES(?,?,?,?)");
		ps.setInt(1, userid);
		ps.setInt(2, groupid);
		ps.setString(3, comment);
		ps.setDate(4, date);
		
		out.println(ps.executeUpdate());
		
	}catch(Exception e){
		
	}
%>