<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.vls.dao.*,com.vls.beans.*,java.util.*,java.sql.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="#" type="../image/png">

<title>Create Group &amp; Manage Group Members</title>



<!--dynamic table-->
<link href="../js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
<link href="../js/advanced-datatable/css/demo_table.css"
	rel="stylesheet" />
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />

<!--ios7-->
<link rel="stylesheet" type="text/css"
	href="../js/ios-switch/switchery.css" />


<!--multi-select-->
<link rel="stylesheet" type="text/css"
	href="../js/jquery-multi-select/css/multi-select.css" />

<!--file upload-->
<link rel="stylesheet" type="text/css"
	href="../css/bootstrap-fileupload.min.css" />

<!--tags input-->
<link rel="stylesheet" type="text/css"
	href="../js/jquery-tags-input/jquery.tagsinput.css" />


<link href="../css/style.css" rel="stylesheet">
<link href="../css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->

</head>


<jsp:useBean id="group" class="com.vls.beans.Group"></jsp:useBean>
<jsp:setProperty property="*" name="group" />


<%
	User user = new User();
	Connection con = DBConnection.getConnection();
	PreparedStatement ps;
	ResultSet rs;

	int userid = 0;

	if (session.getAttribute("userid") == null) {
		response.sendRedirect("index.jsp");
	} else {
		userid = Integer.parseInt(session.getAttribute("userid").toString());
		user = UserManagement.getUser(userid);
		request.setAttribute("user", user);

	}

	if (request.getParameter("submit1") != null) {

		int x = GroupManagement.addGroupWithDetails(group);

		if (request.getParameter("edit") == null) {
			int gid = GroupManagement.getLastGroupId();
			int members[] = {Integer.parseInt(session.getAttribute("userid").toString())};
			x = GroupManagement.addMembers(gid, members);
			if (x == 0)
				GroupManagement.removeGroup(gid);
		}

		if (x != 0)
			response.sendRedirect("creategroup.jsp?success=1");
		else {
			response.sendRedirect("creategroup.jsp?error=1");
		}
	}

	if (request.getParameter("submit3") != null) {
		int gid = Integer.parseInt(request.getParameter("group"));

		int x = 0;

		if (request.getParameter("members") != null) {
			String values[] = request.getParameterValues("members");

			int members[] = new int[values.length];

			for (int i = 0; i < values.length; i++) {
				members[i] = Integer.parseInt(values[i]);

			}

			x = GroupManagement.addMembers(gid, members);
		} else
			x = GroupManagement.removeMembers(gid);

		if (x != 0)
			response.sendRedirect("creategroup.jsp?success=3");
		else
			response.sendRedirect("creategroup.jsp?error=3");

	}

	if (request.getParameter("edit") != null) {
		int gid = Integer.parseInt(request.getParameter("edit"));

		Group editgroup = GroupManagement.getGroup(gid);

		request.setAttribute("editgroup", editgroup);
	}

	List<User> studentsInGroup = new ArrayList<User>();

	if (request.getParameter("gid") != null) {
		int gid = Integer.parseInt(request.getParameter("gid"));

		studentsInGroup = UserManagement.getUsersInGroup(gid);

	}

	List<Group> groups = GroupManagement.getUserGroup(userid);

	List<Group> groupsWithoutSupervisor = (request.getParameter("changesp") != null)
			? GroupManagement.getAllGroups(Integer.parseInt(request.getParameter("changesp")))
			: GroupManagement.getAllGroups(0);

	List<User> students = UserManagement.getAllUsers(1);
	List<User> teachers = UserManagement.getAllUsers(2);

	List<User> studentsNotInGroup = UserManagement.getUsersNoGroup();

	request.setAttribute("groups", groups);

	request.setAttribute("groupsWithoutSupervisor", groupsWithoutSupervisor);

	request.setAttribute("students", students);
	request.setAttribute("teachers", teachers);

	request.setAttribute("studentsNotInGroup", studentsNotInGroup);
	request.setAttribute("studentsInGroup", studentsInGroup);

	Random rand = new Random();

	int ranid;

	if (teachers.size() != 0)
		ranid = teachers.get(rand.nextInt(teachers.size())).getId();
	else
		ranid = -1;

	request.setAttribute("ranid", ranid);
%>



<body class="sticky-header">

	<section>
		<!-- left side start-->
		<div class="left-side sticky-left-side">

			<!--logo and iconic logo start-->
			<div class="logo">
				<a href="index.html"><img src="../images/logo2.png" alt=""></a>
			</div>

			<div class="logo-icon text-center">
				<a href="index.html"><img src="../images/logo_icon.png" alt=""></a>
			</div>
			<!--logo and iconic logo end-->


			<div class="left-side-inner">

				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">

					<li><a href="profile.jsp"><i class="fa fa-bullhorn"></i> <span>Profile</span></a></li>
					<li><a href="editprofile.jsp"><i class="fa fa-bullhorn"></i>
							<span>Edit Profile</span></a></li>

					<li><a href="creategroup.jsp"><i class="fa fa-bullhorn"></i><span>Create
								Group</span></a></li>

					<li class="menu-list"><a href=""><i class="fa fa-laptop"></i><span>Projects</span></a>
						<ul class="sub-menu-list">
							<li><a href="uploadproject.jsp">Upload Project</a></li>
						</ul></li>

					<li><a href="chat.jsp"><i class="fa fa-envelope"></i><span>Chat</span></a></li>

				</ul>
				<!--sidebar nav end-->

			</div>
		</div>
		<!-- left side end-->

		<!-- main content start-->
		<div class="main-content">

			<!-- header section start-->
			<div class="header-section">

				<!--toggle button start-->
				<a class="toggle-btn"><i class="fa fa-bars"></i></a>
				<!--toggle button end-->


				<!--notification menu start -->
				<div class="menu-right">
					<ul class="notification-menu">

						<li><a href="#" class="btn btn-default dropdown-toggle"
							data-toggle="dropdown"> <img
								src="../temp/GetImage?id=${user.getId()}" alt="" />
								${user.getName() } <span class="caret"></span>
						</a>
							<ul class="dropdown-menu dropdown-menu-usermenu pull-right">
								<li><a href="profile.jsp"><i class="fa fa-user"></i>
										Profile</a></li>
								<li><a href="editprofile.jsp"><i class="fa fa-cog"></i>
										Edit Profile</a></li>
								<li><a href="lockscreen.jsp"><i
										class="fa fa-sign-out-alt"></i> Log Out</a></li>
							</ul></li>

					</ul>
				</div>
				<!--notification menu end -->

			</div>
			<!-- header section end-->

			<!--body wrapper start-->
			<div class="wrapper">

				<div class="row">


					<div class="col-lg-5">
						<div class="col-lg-12">

							<c:if test="${param.success==1}">
								<div class="alert alert-success alert-block fade in">
									<button type="button" class="close close-sm"
										data-dismiss="alert">
										<i class="fa fa-times"></i>
									</button>
									<h4>
										<i class="icon-ok-sign"></i> Success!
									</h4>
									<p>Group created successfully ..</p>
								</div>
							</c:if>

							<c:if test="${param.error==1}">
								<div class="alert alert-block alert-danger fade in">
									<button type="button" class="close close-sm"
										data-dismiss="alert">
										<i class="fa fa-times"></i>
									</button>
									<strong>Oh snap!</strong> Sorry you are member of another group
								</div>
							</c:if>

							<section class="panel">
								<header class="panel-heading">
									Create Group <span class="tools pull-right"> <a
										href="javascript:;" class="fa fa-chevron-down"></a> <a
										href="javascript:;" class="fa fa-times"></a>
									</span>

								</header>
								<div class="panel-body">
									<form role="form" method="post" action="creategroup.jsp">

										<input type="hidden" value="${param.edit!=null?param.edit:0 }"
											name="id">

										<div class="form-group">
											<label for="exampleInputEmail1">Group Name</label> <input
												type="text" class="form-control" name="groupname"
												value="${editgroup.getGroupname()}" id="exampleInputEmail1"
												placeholder="Enter groupname" required>
										</div>

										<div class="form-group ">
											<label for="cname">Project Title (required)</label> <input
												class=" form-control" id="cname" name="projectname"
												value="${editgroup.getProjectname()}" 
												type="text" id="exampleInputEmail1"
												placeholder="Enter Project Title" required />
										</div>

										<div class="form-group ">
											<label for="ccomment">Project Details (required)</label>
											<textarea class="form-control" id="exampleInputEmail1"
												style="resize: none;" rows="5"
												placeholder="Enter Project Details" id="ccomment"
												name="projectdetails" required>${editgroup.getProjectdetails()}</textarea>
										</div>

										<button type="submit" name="submit1" value="submit"
											class="btn btn-primary">${param.edit!=null?"Update":"Submit"}</button>
									</form>

								</div>
							</section>
						</div>

					</div>
					<div class="col-lg-7">
						<c:if test="${param.success==3}">
							<div class="alert alert-success alert-block fade in">
								<button type="button" class="close close-sm"
									data-dismiss="alert">
									<i class="fa fa-times"></i>
								</button>
								<h4>
									<i class="icon-ok-sign"></i> Success!
								</h4>
								<p>Operation Executed...</p>
							</div>
						</c:if>

						<c:if test="${param.error==3}">
							<div class="alert alert-block alert-danger fade in">
								<button type="button" class="close close-sm"
									data-dismiss="alert">
									<i class="fa fa-times"></i>
								</button>
								<strong>Oh snap!</strong> Try again..
							</div>
						</c:if>
						<section class="panel">
							<header class="panel-heading">
								Add or Remove group members <span class="tools pull-right"> <a
									href="javascript:;" class="fa fa-chevron-down"></a> <a
									href="javascript:;" class="fa fa-times"></a>
								</span>

							</header>
							<div class="panel-body">
								<form class="form-horizontal" role="form">
									<div class="form-group">
										<label for="inputEmail1"
											class="col-lg-2 col-sm-2 control-label">Group</label>
										<div class="col-lg-10">
											<select name="group" class="form-control m-bot15" id="gid"
												onchange="getUsers();" required>
												<option value="">Select Group</option>
												<c:forEach items="${groups}" var="g">
													<option ${param.gid==g.getId()?"selected":"" }
														value="${g.getId()}">${g.getGroupname()}</option>
												</c:forEach>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label class="col-lg-2 col-sm-2 control-label">Add or Remove Members</label>

										<div class="col-lg-10">
											<select name="members" class="multi-select" multiple="multiple"
												id="my_multi_select3" required>

												<c:forEach items="${studentsInGroup}" var="s">
													<option value="${s.getId()}" selected>${s.getName()}</option>
												</c:forEach>

												<c:forEach items="${studentsNotInGroup}" var="s">
													<option value="${s.getId()}">${s.getName()}</option>
												</c:forEach>

											</select>
										</div>
									</div>

									<div class="form-group">
										<div class="col-lg-offset-2 col-lg-10">
											<button type="submit" name="submit3" value="submit"
												class="btn btn-primary">Submit</button>
										</div>
									</div>
								</form>
							</div>
						</section>

					</div>
				</div>

				<div class="row">
					<div class="col-sm-12">
						<section class="panel">
							<header class="panel-heading">
								Group Information <span class="tools pull-right"> <a
									href="javascript:;" class="fa fa-chevron-down"></a> <a
									href="javascript:;" class="fa fa-times"></a>
								</span>
							</header>
							<div class="panel-body">
								<div class="adv-table">
									<table class="display table table-bordered table-striped"
										id="dynamic-table">
										<thead>
											<tr>
												<th>Serial</th>
												<th>Group Name</th>
												<th>Project Name</th>
												<th>Supervisor</th>
												<th>Members</th>
												<th>Marks</th>
												<th>Action</th>
											</tr>
										</thead>

										<tbody>

											<%
												ps = con.prepareStatement(
														"SELECT G.*, U.name FROM ((groupmembers as GM inner join groups as G on G.id=GM.groupid) inner join users as U on U.id = G.supervisor) where GM.memberid=?");
												ps.setInt(1, userid);
												rs = ps.executeQuery();
												boolean flag = true;

												int i = 1;
												while (rs.next()) {
													flag = false;
											%>
											<tr class="gradeX">
												<td><%=i++%></td>
												<td><%=rs.getString("groupname")%></td>
												<td><%=rs.getString("projectname")%></td>
												<td><%=rs.getString("name")%></td>

												<%
													ps = con.prepareStatement(
																"SELECT U.name FROM users AS U INNER JOIN groupmembers AS GM WHERE U.id = GM.memberid and GM.groupid=?");
														ps.setInt(1, rs.getInt("id"));

														ResultSet rs2 = ps.executeQuery();
														String str = "";
														while (rs2.next()) {
															if (str != "")
																str += ",";
															str += rs2.getString("name");
														}
												%>

												<td><%=str%></td>
												<td><%=rs.getInt("marks")%></td>
												<td><a href="creategroup.jsp?edit=<%=rs.getInt("id")%>"
													class="btn btn-info">Edit</a></td>
											</tr>

											<%
												}
											%>

											<%
												if (flag) {
													ps = con.prepareStatement(
															"SELECT G.* FROM (groupmembers as GM inner join groups as G on G.id=GM.groupid)  where GM.memberid=?");
													ps.setInt(1, userid);
													rs = ps.executeQuery();

													while (rs.next()) {
											%>
											<tr class="gradeX">
												<td><%=i++%></td>
												<td><%=rs.getString("groupname")%></td>
												<td><%=rs.getString("projectname")%></td>
												<td></td>

												<%
													ps = con.prepareStatement(
																	"SELECT U.name FROM users AS U INNER JOIN groupmembers AS GM WHERE U.id = GM.memberid and GM.groupid=?");
															ps.setInt(1, rs.getInt("id"));

															ResultSet rs2 = ps.executeQuery();
															String str = "";
															while (rs2.next()) {
																if (str != "")
																	str += ",";
																str += rs2.getString("name");
															}
												%>

												<td><%=str%></td>
												<td><%=rs.getInt("marks")%></td>
												<td><a href="creategroup.jsp?edit=<%=rs.getInt("id")%>"
													class="btn btn-info">Edit</a></td>
												
											</tr>

											<%
												}
												}
											%>

										</tbody>

										<tfoot>
											<tr>
												<th>Serial</th>
												<th>Group Name</th>
												<th>Project Name</th>
												<th>Supervisor</th>
												<th>Members</th>
												<th>Marks</th>
												<th>Action</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</section>
					</div>
				</div>

			</div>
			<!--body wrapper end-->

			<!--footer section start-->
			<footer class="sticky-footer"> &copy; Developed by Rabeya
				Bashri, Prianti Chowdhury, Arpita Biswas </footer>
			<!--footer section end-->


		</div>
		<!-- main content end-->
	</section>

	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="../js/jquery-1.10.2.min.js"></script>
	<script src="../js/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="../js/jquery-migrate-1.2.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/modernizr.min.js"></script>
	<script src="../js/jquery.nicescroll.js"></script>

	<!--ios7-->
	<script src="../js/ios-switch/switchery.js"></script>
	<script src="../js/ios-switch/ios-init.js"></script>

	<!--icheck -->
	<script src="../js/iCheck/jquery.icheck.js"></script>
	<script src="../js/icheck-init.js"></script>
	<!--multi-select-->
	<script type="text/javascript"
		src="../js/jquery-multi-select/js/jquery.multi-select.js"></script>
	<script type="text/javascript"
		src="../js/jquery-multi-select/js/jquery.quicksearch.js"></script>
	<script src="../js/multi-select-init.js"></script>
	<!--spinner-->
	<script type="text/javascript" src="../js/fuelux/js/spinner.min.js"></script>
	<script src="../js/spinner-init.js"></script>
	<!--file upload-->
	<script type="text/javascript" src="../js/bootstrap-fileupload.min.js"></script>
	<!--tags input-->
	<script src="../js/jquery-tags-input/jquery.tagsinput.js"></script>
	<script src="../js/tagsinput-init.js"></script>
	<!--bootstrap input mask-->
	<script type="text/javascript"
		src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>


	<!--dynamic table-->
	<script type="text/javascript" language="javascript"
		src="../js/advanced-datatable/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../js/data-tables/DT_bootstrap.js"></script>
	<!--dynamic table initialization -->
	<script src="../js/dynamic_table_init.js"></script>

	<!--common scripts for all pages-->
	<script src="../js/scripts.js"></script>

	<script type="text/javascript">
		function getUsers() {
			var gid = document.getElementById('gid').value;

			//var cid = document.getElementById('cid').value;
			if (gid) {
				window.location = 'creategroup.jsp?gid=' + gid;
			} else {
				window.location = 'creategroup.jsp';
			}
		}
	</script>

</body>
</html>