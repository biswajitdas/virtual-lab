<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.vls.dao.*,com.vls.beans.*,java.util.*,java.sql.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="#" type="../image/png">

<title>Upload Project Files</title>

<!--dynamic table-->
<link href="../js/advanced-datatable/css/demo_page.css" rel="stylesheet" />
<link href="../js/advanced-datatable/css/demo_table.css"
	rel="stylesheet" />
<link rel="stylesheet" href="../js/data-tables/DT_bootstrap.css" />

<!--ios7-->
<link rel="stylesheet" type="text/css"
	href="../js/ios-switch/switchery.css" />


<!--multi-select-->
<link rel="stylesheet" type="text/css"
	href="../js/jquery-multi-select/css/multi-select.css" />

<!--file upload-->
<link rel="stylesheet" type="text/css"
	href="../css/bootstrap-fileupload.min.css" />

<!--tags input-->
<link rel="stylesheet" type="text/css"
	href="../js/jquery-tags-input/jquery.tagsinput.css" />


<link href="../css/style.css" rel="stylesheet">
<link href="../css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->


<script>
	function insertdata() {

		var userid = document.getElementById("userid").value;
		var groupid = document.getElementById("groupid").value;
		var comment = document.getElementById("ccomment").value;

		var xmlhttp = new XMLHttpRequest();
		xmlhttp.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
				//document.getElementById("txtHint").innerHTML = this.responseText;
			}
		};

		xmlhttp.open("GET", "insertdata.jsp?userid=" + userid + "&groupid="
				+ groupid + "&comment=" + comment, true);
		xmlhttp.send();

		return true;
	}
</script>

</head>

<%
	int groupid = 0;
	int memberid = 0;

	List<Group> groups = null;

	User user = new User();
	if (session.getAttribute("userid") == null) {
		response.sendRedirect("index.jsp");
	} else {
		memberid = Integer.parseInt(session.getAttribute("userid").toString());
		groupid = GroupManagement.getUserGroupId(memberid);

		if (groupid != 0) {
			groups = GroupManagement.getAllGroupOfUser(memberid);
			request.setAttribute("groups", groups);
		}

		user = UserManagement.getUser(memberid);
		request.setAttribute("user", user);
	}
%>




<body class="sticky-header">

	<section>
		<!-- left side start-->
		<div class="left-side sticky-left-side">

			<!--logo and iconic logo start-->
			<div class="logo">
				<a href="index.html"><img src="../images/logo2.png" alt=""></a>
			</div>

			<div class="logo-icon text-center">
				<a href="index.html"><img src="../images/logo_icon.png" alt=""></a>
			</div>
			<!--logo and iconic logo end-->


			<div class="left-side-inner">

				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">

					<li><a href="profile.jsp"><i class="fa fa-bullhorn"></i> <span>Profile</span></a></li>
					<li><a href="editprofile.jsp"><i class="fa fa-bullhorn"></i>
							<span>Edit Profile</span></a></li>

					<li><a href="creategroup.jsp"><i class="fa fa-bullhorn"></i><span>Create
								Group</span></a></li>

					<li class="menu-list"><a href=""><i class="fa fa-laptop"></i><span>Projects</span></a>
						<ul class="sub-menu-list">
							<li><a href="uploadproject.jsp">Upload Project</a></li>
						</ul></li>

					<li><a href="chat.jsp"><i class="fa fa-envelope"></i><span>Chat</span></a></li>

				</ul>
				<!--sidebar nav end-->

			</div>
		</div>
		<!-- left side end-->

		<!-- main content start-->
		<div class="main-content">

			<!-- header section start-->
			<div class="header-section">

				<!--toggle button start-->
				<a class="toggle-btn"><i class="fa fa-bars"></i></a>
				<!--toggle button end-->



				<!--notification menu start -->
				<div class="menu-right">
					<ul class="notification-menu">

						<li><a href="#" class="btn btn-default dropdown-toggle"
							data-toggle="dropdown"> <img
								src="../temp/GetImage?id=${user.getId()}" alt="" />
								${user.getName() } <span class="caret"></span>
						</a>
							<ul class="dropdown-menu dropdown-menu-usermenu pull-right">
								<li><a href="profile.jsp"><i class="fa fa-user"></i>
										Profile</a></li>
								<li><a href="editprofile.jsp"><i class="fa fa-cog"></i>
										Edit Profile</a></li>
								<li><a href="lockscreen.jsp"><i
										class="fa fa-sign-out-alt"></i> Log Out</a></li>
							</ul></li>

					</ul>
				</div>
				<!--notification menu end -->

			</div>
			<!-- header section end-->




			<!--body wrapper start-->
			<div class="wrapper">
				<div class="row">

					<div class="col-lg-12">

						<c:if test="${param.success==1}">
							<div class="alert alert-success alert-block fade in">
								<button type="button" class="close close-sm"
									data-dismiss="alert">
									<i class="fa fa-times"></i>
								</button>
								<h4>
									<i class="icon-ok-sign"></i> Success!
								</h4>
								<p>Project uploaded...</p>
							</div>
						</c:if>

						<%
							if (groupid == 0) {
						%>
						<div class="alert alert-block alert-danger fade in">
							<button type="button" class="close close-sm" data-dismiss="alert">
								<i class="fa fa-times"></i>
							</button>
							<strong>Oh snap!</strong> You are not member of any group.
						</div>
						<%
							} else {
						%>

						<section class="panel">
							<header class="panel-heading">
								Form validations <span class="tools pull-right"> <a
									href="javascript:;" class="fa fa-chevron-down"></a> <a
									href="javascript:;" class="fa fa-times"></a>
								</span>
							</header>
							<div class="panel-body">
								<div class=" form">
									<form class="cmxform form-horizontal adminex-form"
										id="commentForm" method="post" enctype="multipart/form-data"
										action="uploadandstore.jsp" onsubmit="insertdata()">

										<input type="hidden" value="<%=memberid%>" name="userid"
											id="userid" />

										<div class="form-group">
											<label for="cgroup" class="control-label col-lg-2">Group
												(required)</label>

											<div class="col-lg-10">
												<select class="form-control m-bot15" name="groupid"
													id="groupid"">
													<option value="">Select Group</option>
													<c:forEach items="${groups}" var="g">
														<c:set var="groupid" value="${g.getId()}"></c:set>
														<option value="${g.getId()}"
															${param.gid==g.getId()?"selected":"" }>${g.getGroupname()}</option>
													</c:forEach>
												</select>
											</div>

										</div>


										<div class="form-group ">
											<label for="ccomment" class="control-label col-lg-2">Your
												Comment (required)</label>
											<div class="col-lg-10">
												<textarea class="form-control " id="ccomment" name="comment"
													required></textarea>
											</div>
										</div>

										<div class="form-group">
											<label for="exampleInputFile2"
												class="col-lg-2 col-sm-2 control-label">File input</label>
											<div class="col-lg-10">
												<input id="exampleInputFile2" type="file" name="file">
												<!-- <p class="help-block">Example block-level help text
													here.</p> -->
											</div>
										</div>

										<div class="form-group">
											<div class="col-lg-offset-2 col-lg-10">
												<button class="btn btn-primary" type="submit">Save</button>
												<button class="btn btn-default" type="button">Cancel</button>
											</div>
										</div>
									</form>
								</div>

							</div>
						</section>
					</div>

					<div class="col-sm-12">
						<section class="panel">
							<header class="panel-heading">
								Project Information <span class="tools pull-right"> <a
									href="javascript:;" class="fa fa-chevron-down"></a> <a
									href="javascript:;" class="fa fa-times"></a>
								</span>
							</header>
							<div class="panel-body">
								<div class="adv-table">
									<table class="display table table-bordered table-striped"
										id="dynamic-table">
										<thead>
											<tr>
												<th>Serial</th>
												<th>Group Name</th>
												<th>Project Name</th>
												<th>Uploader Name</th>
												<th>Comment</th>
												<th>Uploaded At</th>
												<th>Download File</th>

											</tr>
										</thead>

										<tbody>

											<%
												Connection con = DBConnection.getConnection();

													PreparedStatement ps = con.prepareStatement(
															" SELECT UP.filename,UP.comment,UP.date, U.name, G.groupname, G.projectname from ((uploadedprojects as UP inner join groups as G on UP.groupid=G.id) inner join Users as U on U.id=UP.uploader) where UP.groupid=? order by UP.id desc");
													ps.setInt(1, groupid);
													ResultSet rs = ps.executeQuery();
													int i = 1;
													while (rs.next()) {
											%>
											<tr class="gradeX">
												<td><%=i++%></td>
												<td><%=rs.getString("groupname")%></td>
												<td><%=rs.getString("projectname")%></td>
												<td><%=rs.getString("name")%></td>
												<td><%=rs.getString("comment")%></td>
												<td><%=rs.getDate("date").toString()%>
												<td><a
													href="../UploadedFiles/<%=rs.getString("filename")%>"
													download="download" class="btn btn-info">Download</a></td>
											</tr>

											<%
												}
											%>

										</tbody>

										<tfoot>
											<tr>
												<th>Serial</th>
												<th>Group Name</th>
												<th>Project Name</th>
												<th>Uploader Name</th>
												<th>Comment</th>
												<th>Uploaded At</th>
												<th>Download File</th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						</section>
					</div>
				</div>

				<%
					}
				%>

			</div>

		</div>
		<!--body wrapper end-->

		<!--footer section start-->
		<footer class="sticky-footer"> &copy; Developed by Rabeya
			Bashri, Prianti Chowdhury, Arpita Biswas </footer>
		<!--footer section end-->


		</div>
		<!-- main content end-->
	</section>

	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="../js/jquery-1.10.2.min.js"></script>
	<script src="../js/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="../js/jquery-migrate-1.2.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/modernizr.min.js"></script>
	<script src="../js/jquery.nicescroll.js"></script>

	<!--ios7-->
	<script src="../js/ios-switch/switchery.js"></script>
	<script src="../js/ios-switch/ios-init.js"></script>

	<!--icheck -->
	<script src="../js/iCheck/jquery.icheck.js"></script>
	<script src="../js/icheck-init.js"></script>
	<!--multi-select-->
	<script type="text/javascript"
		src="../js/jquery-multi-select/js/jquery.multi-select.js"></script>
	<script type="text/javascript"
		src="../js/jquery-multi-select/js/jquery.quicksearch.js"></script>
	<script src="../js/multi-select-init.js"></script>
	<!--spinner-->
	<script type="text/javascript" src="../js/fuelux/js/spinner.min.js"></script>
	<script src="../js/spinner-init.js"></script>
	<!--file upload-->
	<script type="text/javascript" src="../js/bootstrap-fileupload.min.js"></script>
	<!--tags input-->
	<script src="../js/jquery-tags-input/jquery.tagsinput.js"></script>
	<script src="../js/tagsinput-init.js"></script>
	<!--bootstrap input mask-->
	<script type="text/javascript"
		src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>


	<!--dynamic table-->
	<script type="text/javascript" language="javascript"
		src="../js/advanced-datatable/js/jquery.dataTables.js"></script>
	<script type="text/javascript" src="../js/data-tables/DT_bootstrap.js"></script>
	<!--dynamic table initialization -->
	<script src="../js/dynamic_table_init.js"></script>

	<!--common scripts for all pages-->
	<script src="../js/scripts.js"></script>



</body>
</html>
