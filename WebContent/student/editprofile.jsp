<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.vls.dao.UserManagement,com.vls.beans.User"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="#" type="../image/png">

<title>Edit Profile</title>

<!--ios7-->
<link rel="stylesheet" type="text/css"
	href="../js/ios-switch/switchery.css" />


<!--multi-select-->
<link rel="stylesheet" type="text/css"
	href="../js/jquery-multi-select/css/multi-select.css" />

<!--file upload-->
<link rel="stylesheet" type="text/css"
	href="../css/bootstrap-fileupload.min.css" />

<!--tags input-->
<link rel="stylesheet" type="text/css"
	href="../js/jquery-tags-input/jquery.tagsinput.css" />


<link href="../css/style.css" rel="stylesheet">
<link href="../css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->

</head>

<jsp:useBean id="u" class="com.vls.beans.User"></jsp:useBean>
<jsp:setProperty property="*" name="u" />


<%
	User user = new User();
	if (session.getAttribute("userid") == null) {
		response.sendRedirect("index.jsp");
	} else {
		int id = Integer.parseInt(session.getAttribute("userid").toString());
		user = UserManagement.getUser(id);
		request.setAttribute("user", user);

	}

	if (request.getParameter("cp") != null) {
		int id = Integer.parseInt(session.getAttribute("userid").toString());

		int x = UserManagement.updatePassword(id, request.getParameter("oldpass"),
				request.getParameter("newpass"));

		if (x != 0)
			response.sendRedirect("editprofile.jsp?success=1");
	}
%>




<body class="sticky-header">

	<section>
		<!-- left side start-->
		<div class="left-side sticky-left-side">

			<!--logo and iconic logo start-->
			<div class="logo">
				<a href="index.html"><img src="../images/logo2.png" alt=""></a>
			</div>

			<div class="logo-icon text-center">
				<a href="index.html"><img src="../images/logo_icon.png" alt=""></a>
			</div>
			<!--logo and iconic logo end-->


			<div class="left-side-inner">

				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">

					<li><a href="profile.jsp"><i class="fa fa-bullhorn"></i> <span>Profile</span></a></li>
					<li><a href="editprofile.jsp"><i class="fa fa-bullhorn"></i> <span>Edit
								Profile</span></a></li>

					<li><a href="creategroup.jsp"><i class="fa fa-bullhorn"></i><span>Create
								Group</span></a></li>

					<li class="menu-list"><a href=""><i class="fa fa-laptop"></i><span>Projects</span></a>
						<ul class="sub-menu-list">
							<li><a href="uploadproject.jsp">Upload Project</a></li>
						</ul></li>

					<li><a href="chat.jsp"><i class="fa fa-envelope"></i><span>Chat</span></a></li>
				</ul>
				<!--sidebar nav end-->

			</div>
		</div>
		<!-- left side end-->

		<!-- main content start-->
		<div class="main-content">

			<!-- header section start-->
			<div class="header-section">

				<!--toggle button start-->
				<a class="toggle-btn"><i class="fa fa-bars"></i></a>
				<!--toggle button end-->

				<!--notification menu start -->
				<div class="menu-right">
					<ul class="notification-menu">
						
						<li><a href="#" class="btn btn-default dropdown-toggle"
							data-toggle="dropdown"> <img
								src="../temp/GetImage?id=${user.getId()}" alt="" />
								${user.getName() } <span class="caret"></span>
						</a>
							<ul class="dropdown-menu dropdown-menu-usermenu pull-right">
								<li><a href="profile.jsp"><i class="fa fa-user"></i>
										Profile</a></li>
								<li><a href="editprofile.jsp"><i class="fa fa-cog"></i>
										Edit Profile</a></li>
								<li><a href="lockscreen.jsp"><i
										class="fa fa-sign-out-alt"></i> Log Out</a></li>
							</ul></li>

					</ul>
				</div>
				<!--notification menu end -->

			</div>
			<!-- header section end-->

			<!--body wrapper start-->
			<div class="wrapper">
				<div class="row">

					<c:if test="${param.success==1}">
						<div class="alert alert-success alert-block fade in">
							<button type="button" class="close close-sm" data-dismiss="alert">
								<i class="fa fa-times"></i>
							</button>
							<h4>
								<i class="icon-ok-sign"></i> Success!
							</h4>
							<p>Profile Updated...</p>
						</div>
					</c:if>

					<div class="col-lg-6">

						<section class="panel">
							<header class="panel-heading">
								Edit Profile <span class="tools pull-right"> <a
									href="javascript:;" class="fa fa-chevron-down"></a> <a
									href="javascript:;" class="fa fa-times"></a>
								</span>
							</header>
							<div class="panel-body">
								<div class=" form">
									<form class="cmxform form-horizontal adminex-form"
										method="post" action="uploadServlet"
										enctype="multipart/form-data">

										<input type="hidden" value="${sessionScope.userid}" name="id" />
										<input type="hidden" value="${user.getBatch()}" name="batch" />
										<input type="hidden" value="${user.getDesignation()}"
											name="designation" /> <input type="hidden"
											value="${user.getType()}" name="type" />


										<div class="form-group ">
											<label for="cname" class="control-label col-lg-2">Name
												(required)</label>
											<div class="col-lg-10">
												<input class=" form-control" id="cname"
													value="${user.getName()}" name="name" minlength="2"
													type="text" required />
											</div>
										</div>


										<div class="form-group ">
											<label for="cdept" class="control-label col-lg-2">Department
												(required)</label>
											<div class="col-lg-10">
												<select class="form-control m-bot15" name="department"
													id="cdept" required>
													<option ${user.getDepartment()=="CSE"?"selected":""}
														value="CSE">CSE</option>
													<option ${user.getDepartment()=="EEE"?"selected":""}
														value="EEE">EEE</option>
												</select>
											</div>
										</div>
										<div class="form-group ">
											<label for="" class="control-label col-lg-2">Gender
												(required)</label>
											<div class="radios col-lg-10">
												<label for="radio-01" class="label_radio col-lg-6 col-sm-6">
													<input type="radio" value="1"
													${user.getGender()==1?"checked":"" } id="radio-01"
													name="gender" required="required"> Male
												</label> <label for="radio-02" class="label_radio col-lg-6 col-sm-6">
													<input type="radio" value="2"
													${user.getGender()==2?"checked":"" } id="radio-02"
													name="gender" required="required"> Female
												</label>
											</div>
										</div>
										<div class="form-group ">
											<label for="" class="control-label col-lg-2">Select
												Image (optional)</label>
											<div class="col-lg-10 col-sm-10">
												<div class="fileupload fileupload-new"
													data-provides="fileupload">
													<input value="" name="" type="hidden">
													<div class="fileupload-new thumbnail"
														style="width: 200px; height: 150px;">
														<img src="../temp/GetImage?id=${user.getId()}" alt="">
													</div>
													<div class="fileupload-preview fileupload-exists thumbnail"
														style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
													<div>
														<span class="btn btn-default btn-file"> <span
															class="fileupload-new"><i class="fa fa-paper-clip"></i>
																Select image</span> <span class="fileupload-exists"><i
																class="fa fa-undo"></i> Change</span> <input class="default"
															name="image" type="file">
														</span> <a href="#" class="btn btn-danger fileupload-exists"
															data-dismiss="fileupload"><i class="fa fa-trash"></i>
															Remove</a>
													</div>
												</div>
												
											</div>
										</div>


										<div class="form-group ">
											<label for="cemail" class="control-label col-lg-2">E-Mail
												(required)</label>
											<div class="col-lg-10">
												<input class="form-control " id="cemail"
													value="${user.getEmail()}" type="email" name="email"
													required />
											</div>
										</div>

										<div class="form-group">
											<label class=" col-md-2 control-label">Skills</label>
											<div class="col-md-10">
												<input id="tags_1" type="text" name="skills" class="tags"
													value="${user.getSkills()}" />
											</div>
										</div>

										<div class="form-group ">
											<label for="caddress" class="control-label col-lg-2">Address
												(required)</label>
											<div class="col-lg-10">
												<input class="form-control " id="caddress"
													value="${user.getAddress()}" type="text" name="address"
													required />
											</div>
										</div>

										<div class="form-group ">
											<label for="ccomment" class="control-label col-lg-2">Your
												Status (required)</label>
											<div class="col-lg-10">
												<textarea class="form-control" rows="6" id="ccomment"
													name="status" required>${user.getStatus()}</textarea>
											</div>
										</div>

										<div class="form-group">
											<div class="col-lg-offset-2 col-lg-10">
												<input class="btn btn-primary" name="submit" value="Submit"
													type="submit">

											</div>
										</div>
									</form>
								</div>

							</div>
						</section>
					</div>
					<div class="col-lg-6">
						<section class="panel">
							<header class="panel-heading">
								Change Password <span class="tools pull-right"> <a
									href="javascript:;" class="fa fa-chevron-down"></a> <a
									href="javascript:;" class="fa fa-times"></a>
								</span>
							</header>
							<div class="panel-body">
								<div class="form">
									<form class="cmxform form-horizontal adminex-form"
										method="post" action="editprofile.jsp">

										<div class="form-group ">
											<label for="username" class="control-label col-lg-2">Username</label>
											<div class="col-lg-10">
												<input class="form-control" value="${user.getUsername()}"
													id="username" name="username" readonly="readonly"
													type="text" />
											</div>
										</div>
										<div class="form-group ">
											<label for="oldpass" class="control-label col-lg-2">Old
												Password</label>
											<div class="col-lg-10">
												<input class="form-control " id="oldpass" name="oldpass"
													type="password" required/>
											</div>
										</div>
										<div class="form-group ">
											<label for="newpass" class="control-label col-lg-2">New
												Password</label>
											<div class="col-lg-10">
												<input class="form-control " id="newpass" name="newpass"
													type="password" required/>
											</div>
										</div>
										<div class="form-group">
											<div class="col-lg-offset-2 col-lg-10">
												<input class="btn btn-primary" name="cp"
													value="Change Password" type="submit">
											</div>
										</div>
									</form>
								</div>
							</div>
						</section>
					</div>
				</div>


			</div>
			<!--body wrapper end-->

			<!--footer section start-->
			<footer class="sticky-footer"> &copy; Developed by Rabeya
				Bashri, Prianti Chowdhury, Arpita Biswas </footer>
			<!--footer section end-->


		</div>
		<!-- main content end-->
	</section>

	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="../js/jquery-1.10.2.min.js"></script>
	<script src="../js/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="../js/jquery-migrate-1.2.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/modernizr.min.js"></script>
	<script src="../js/jquery.nicescroll.js"></script>

	<!--ios7-->
	<script src="../js/ios-switch/switchery.js"></script>
	<script src="../js/ios-switch/ios-init.js"></script>

	<!--icheck -->
	<script src="../js/iCheck/jquery.icheck.js"></script>
	<script src="../js/icheck-init.js"></script>
	<!--multi-select-->
	<script type="text/javascript"
		src="../js/jquery-multi-select/js/jquery.multi-select.js"></script>
	<script type="text/javascript"
		src="../js/jquery-multi-select/js/jquery.quicksearch.js"></script>
	<script src="../js/multi-select-init.js"></script>
	<!--spinner-->
	<script type="text/javascript" src="../js/fuelux/js/spinner.min.js"></script>
	<script src="../js/spinner-init.js"></script>
	<!--file upload-->
	<script type="text/javascript" src="../js/bootstrap-fileupload.min.js"></script>
	<!--tags input-->
	<script src="../js/jquery-tags-input/jquery.tagsinput.js"></script>
	<script src="../js/tagsinput-init.js"></script>
	<!--bootstrap input mask-->
	<script type="text/javascript"
		src="../js/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>


	<!--common scripts for all pages-->
	<script src="../js/scripts.js"></script>
	.


</body>
</html>
