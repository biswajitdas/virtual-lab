<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.vls.dao.*,com.vls.beans.User,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="#" type="../image/png">

<title>Chat with supervisor &amp; Group Members</title>

<link href="../css/style.css" rel="stylesheet">
<link href="../css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
  <![endif]-->
</head>

<jsp:useBean id="message" class="com.vls.beans.Message"></jsp:useBean>
<jsp:setProperty property="*" name="message" />


<%
	int groupid = 0;
	int memberid = 0;
	
	User user = new User();

	if (session.getAttribute("userid") == null) {
		response.sendRedirect("index.jsp");
	} else {
		memberid = Integer.parseInt(session.getAttribute("userid").toString());
		groupid = GroupManagement.getUserGroupId(memberid);
		user = UserManagement.getUser(memberid);
		request.setAttribute("user", user);
		
		
		
	}

	if (request.getParameter("submit") != null) {
		int x = ChatManagement.postMessage(message);

/* 		if (x != 0) {
			response.sendRedirect("chat.jsp?success=1");
		} else {
			response.sendRedirect("chat.jsp?error=1");
		} */

	}
	
	
%>

<body class="sticky-header">

	<section>
		<!-- left side start-->
		<div class="left-side sticky-left-side">

			<!--logo and iconic logo start-->
			<div class="logo">
				<a href="index.html"><img src="../images/logo2.png" alt=""></a>
			</div>

			<div class="logo-icon text-center">
				<a href="index.html"><img src="../images/logo_icon.png" alt=""></a>
			</div>
			<!--logo and iconic logo end-->


			<div class="left-side-inner">
				
				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">

					<li><a href="profile.jsp"><i class="fa fa-bullhorn"></i> <span>Profile</span></a></li>
					<li><a href="editprofile.jsp"><i class="fa fa-bullhorn"></i>
							<span>Edit Profile</span></a></li>

					<li><a href="creategroup.jsp"><i class="fa fa-bullhorn"></i><span>Create
								Group</span></a></li>

					<li class="menu-list"><a href=""><i class="fa fa-laptop"></i><span>Projects</span></a>
						<ul class="sub-menu-list">
							<li><a href="uploadproject.jsp">Upload Project</a></li>
						</ul></li>

					<li><a href="chat.jsp"><i class="fa fa-envelope"></i><span>Chat</span></a></li>

				</ul>
				<!--sidebar nav end-->

			</div>
		</div>
		<!-- left side end-->

		<!-- main content start-->
		<div class="main-content">

			<!-- header section start-->
			<div class="header-section">

				<!--toggle button start-->
				<a class="toggle-btn"><i class="fa fa-bars"></i></a>
				<!--toggle button end-->

				<!--notification menu start -->
				<div class="menu-right">
					<ul class="notification-menu">
						
						<li><a href="#" class="btn btn-default dropdown-toggle"
							data-toggle="dropdown"> <img
								src="../temp/GetImage?id=${user.getId()}" alt="" />
								${user.getName() } <span class="caret"></span>
						</a>
							<ul class="dropdown-menu dropdown-menu-usermenu pull-right">
								<li><a href="profile.jsp"><i class="fa fa-user"></i>
										Profile</a></li>
								<li><a href="editprofile.jsp"><i class="fa fa-cog"></i>
										Edit Profile</a></li>
								<li><a href="lockscreen.jsp"><i
										class="fa fa-sign-out-alt"></i> Log Out</a></li>
							</ul></li>

					</ul>
				</div>
				<!--notification menu end -->

			</div>
			<!-- header section end-->

			<!--body wrapper start-->
			<div class="wrapper">
				<div class="row">
				
				<% if(groupid==0){ %>
				<div class="alert alert-block alert-danger fade in">
                                <button type="button" class="close close-sm" data-dismiss="alert">
                                    <i class="fa fa-times"></i>
                                </button>
                                <strong>Oh snap!</strong> you are not member of any group.
                            </div>
				<%} else { %>
					<div class="col-md-6">
						<div class="panel">
							<header class="panel-heading">
								Chat with supervisor <span class="tools pull-right"> <a
									href="javascript:;" class="fa fa-chevron-down"></a> <a
									href="javascript:;" class="fa fa-times"></a>
								</span>
							</header>
							<div class="panel-body">
								<ul class="chats normal-chat"
									style="height: 450px; overflow: auto;" id="chathistory1">


								</ul>
								<div class="chat-form ">
									<form role="form" class="form-inline" method="post">
										<div class="form-group">
											<input type="hidden" value="<%=memberid%>" name="userid" />
											<input type="hidden" value="<%=groupid%>" name="groupid" />
											<input type="hidden" value="1" name="withteacher" />
											
											<input type="text" style="width: 100%" name="message"
												placeholder="Type a message here..." class="form-control">
										</div>
										<button class="btn btn-primary" name="submit" value="submit"
											type="submit">Send</button>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel">
							<header class="panel-heading">
								Chat with group members <span class="tools pull-right"> <a
									href="javascript:;" class="fa fa-chevron-down"></a> <a
									href="javascript:;" class="fa fa-times"></a>
								</span>
							</header>
							<div class="panel-body">
								<ul class="chats cool-chat"
									style="height: 450px; overflow: auto;" id="chathistory2">

								</ul>

								<div class="chat-form ">
									<form role="form" class="form-inline" method="post">
										<div class="form-group">
											<input type="hidden" value="<%=memberid%>" name="userid" />
											<input type="hidden" value="<%=groupid%>" name="groupid" />
											<input type="hidden" value="0" name="withteacher" />
											
											<input type="text" style="width: 100%" name="message"
												placeholder="Type a message here..." class="form-control">
										</div>
										<button class="btn btn-primary" name="submit" value="submit"
											type="submit">Send</button>
									</form>
								</div>
							</div>
						</div>
					</div>
					
					<% } %>
				</div>
			</div>
			<!--body wrapper end-->

			<!--footer section start-->
			<footer class="sticky-footer"> &copy; Developed by Rabeya
				Bashri, Prianti Chowdhury, Arpita Biswas </footer>
			<!--footer section end-->


		</div>
		<!-- main content end-->
	</section>

	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="../js/jquery-1.10.2.min.js"></script>
	<script src="../js/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="../js/jquery-migrate-1.2.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/modernizr.min.js"></script>
	<script src="../js/jquery.nicescroll.js"></script>


	<!--common scripts for all pages-->
	<script src="../js/scripts.js"></script>


	<script>
    setInterval(loadDoc1, 500);

	loadDoc1();
	
    function loadDoc1() {


            var textarea = document.getElementById("chathistory1");

            textarea.style.overflow = "auto";

            textarea.scrollTop = textarea.scrollHeight-200;

        var xhttp;
        if (window.XMLHttpRequest) {
            // code for modern browsers
            xhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("chathistory1").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", "chatretrieve.jsp?groupid=<%=groupid%>&memberid=<%=memberid%>&withteacher=1",true);
			xhttp.send();

		}
    
    
    
    setInterval(loadDoc2, 500);

	loadDoc2();
	
    function loadDoc2() {


            var textarea = document.getElementById("chathistory2");

            textarea.style.overflow = "auto";

            textarea.scrollTop = textarea.scrollHeight-200;

        var xhttp;
        if (window.XMLHttpRequest) {
            // code for modern browsers
            xhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("chathistory2").innerHTML = this.responseText;
            }
        };
        xhttp.open("POST", "chatretrieve.jsp?groupid=<%=groupid%>&memberid=<%=memberid%>&withteacher=0", true);
			xhttp.send();

		}
	</script>


</body>
</html>
