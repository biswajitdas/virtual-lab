<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="#" type="image/png">

<title>Login</title>

<link href="css/style.css" rel="stylesheet">
<link href="css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>  
<body class="login-body">

	<div class="container">

		<form class="form-signin" action="index.html">
			<div class="form-signin-heading text-center">
				<h1 class="sign-title">Welcome to Virtual Lab</h1>
				<img src="images/login-logo.png" alt="" />
			</div>
			<div class="login-wrap">
				<a href="student/index.jsp" target="_blank"
					class="btn btn-sm btn-login btn-block" style="font-size:20px" type="submit"> Student
					Panel </a> 
					
					<a href="teacher/index.jsp" target="_blank"
					class="btn btn-sm btn-login btn-block" style="font-size:20px" type="submit"> Teacher
					Panel </a>
					
					 <a href="admin/index.jsp" target="_blank"
					class="btn btn-sm btn-login btn-block" style="font-size:20px" type="submit"> Admin
					Panel </a>

			</div>



		</form>

	</div>



	<!-- Placed js at the end of the document so the pages load faster -->

	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="js/jquery-1.10.2.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/modernizr.min.js"></script>

</body>
</html>
