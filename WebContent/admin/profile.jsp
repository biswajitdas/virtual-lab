<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.vls.dao.UserManagement,com.vls.beans.User"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="#" type="../image/png">

<title>Profile</title>

<link href="../css/style.css" rel="stylesheet">
<link href="../css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
  <![endif]-->
</head>

<%
	User user = new User();
	if (session.getAttribute("adminid") == null) {
		response.sendRedirect("index.jsp");
	} else {
		int id = Integer.parseInt(session.getAttribute("adminid").toString());
		user = UserManagement.getUser(id);
		request.setAttribute("user", user);
	}
%>

<body class="sticky-header">

	<section>
		<!-- left side start-->
		<div class="left-side sticky-left-side">

			<!--logo and iconic logo start-->
			<div class="logo">
				<a href="index.html"><img src="../images/logo2.png" alt=""></a>
			</div>

			<div class="logo-icon text-center">
				<a href="index.html"><img src="../images/logo_icon.png" alt=""></a>
			</div>
			<!--logo and iconic logo end-->


			<div class="left-side-inner">

				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">

					<li><a href="profile.jsp"><i class="fa fa-bullhorn"></i> <span>Profile</span></a></li>
					<li><a href="editprofile.jsp"><i class="fa fa-bullhorn"></i>
							<span>Edit Profile</span></a></li>
					<li><a href="creategroup.jsp"><i class="fa fa-bullhorn"></i>
							<span>Create Group</span></a></li>
					<li><a href="createadmin.jsp"><i class="fa fa-bullhorn"></i>
							<span>Create Admin</span></a></li>

				</ul>
				<!--sidebar nav end-->

			</div>
		</div>
		<!-- left side end-->

		<!-- main content start-->
		<div class="main-content">

			<!-- header section start-->
			<div class="header-section">

				<!--toggle button start-->
				<a class="toggle-btn"><i class="fa fa-bars"></i></a>
				<!--toggle button end-->

				<!--notification menu start -->
				<div class="menu-right">
					<ul class="notification-menu">

						<li><a href="#" class="btn btn-default dropdown-toggle"
							data-toggle="dropdown"> <img
								src="../temp/GetImage?id=${user.getId()}" alt="" />
								${user.getName() } <span class="caret"></span>
						</a>
							<ul class="dropdown-menu dropdown-menu-usermenu pull-right">
								<li><a href="profile.jsp"><i class="fa fa-user"></i>
										Profile</a></li>
								<li><a href="editprofile.jsp"><i class="fa fa-cog"></i>
										Edit Profile</a></li>
								<li><a href="lockscreen.jsp"><i
										class="fa fa-sign-out-alt"></i> Log Out</a></li>
							</ul></li>

					</ul>
				</div>
				<!--notification menu end -->

			</div>
			<!-- header section end-->

			<div class="wrapper">

				<div class="row">
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-body">
										<div class="profile-pic text-center">
											<img alt="" src="../temp/GetImage?id=${user.getId()}">
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-body">
										<ul class="p-info">
											<li>
												<div class="title">Gender</div>
												<div class="desk">${user.getGender()==1?"Male":"Female"}</div>
											</li>
											<li>
												<div class="title">Department</div>
												<div class="desk">${user.getDepartment()}</div>
											</li>
											<li>
												<div class="title">Project Done</div>
												<div class="desk">${user.getTotalprojects()}</div>
											</li>
											<li>
												<div class="title">Skills</div>
												<div class="desk">${user.getSkills() }</div>
											</li>
										</ul>
									</div>
								</div>
							</div>

						</div>
					</div>
					<div class="col-md-8">
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<div class="panel-body">
										<div class="profile-desk">
											<h1>${user.getName()}</h1>
											<span class="designation"> <%
 	switch (user.getDesignation()) {
 		case 1 :
 			out.println("Student");
 			break;
 		case 2 :
 			out.println("Teaching Assistant");
 			break;
 		case 3 :
 			out.println("Lecturer");
 			break;
 		case 4 :
 			out.println("Assistant Professor");
 			break;
 		case 5 :
 			out.println("Associate Professor");
 			break;
 		case 6 :
 			out.println("Professor");
 			break;
 		case 7 :
 			out.println("Admin");
 			break;
 	}
 %>
											</span>
											<p>${user.getStatus()}</p>
											<!--a class="btn p-follow-btn pull-left" href="#"> <i class="fa fa-check"></i> Following</a-->

											<ul class="p-social-link pull-right">
												<li><a href="#"> <i class="fab fa-facebook-f"></i>
												</a></li>
												<li class="active"><a href="#"> <i
														class="fab fa-twitter"></i>
												</a></li>
												<li><a href="#"> <i class="fab fa-google-plus-g"></i>
												</a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<form>
										<textarea class="form-control input-lg p-text-area" rows="2"
											placeholder="Whats in your mind today?"></textarea>
									</form>
									<footer class="panel-footer">
										<button class="btn btn-post pull-right">Post</button>
										<ul class="nav nav-pills p-option">
											<li><a href="#"><i class="fa fa-user"></i></a></li>
											<li><a href="#"><i class="fa fa-camera"></i></a></li>
											<li><a href="#"><i class="fa  fa-location-arrow"></i></a>
											</li>
											<li><a href="#"><i class="fa fa-meh-o"></i></a></li>
										</ul>
									</footer>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel">
									<header class="panel-heading">
										recent activities <span class="tools pull-right"> <a
											class="fa fa-chevron-down" href="javascript:;"></a> <a
											class="fa fa-times" href="javascript:;"></a>
										</span>
									</header>
									<div class="panel-body">
										<ul class="activity-list">
											<li>
												<div class="avatar">
													<img src="../images/photos/user1.png" alt="" />
												</div>
												<div class="activity-desk">
													<h5>
														<a href="#">Jonathan Smith</a> <span>Uploaded 5 new
															photos</span>
													</h5>
													<p class="text-muted">7 minutes ago near Alaska, USA</p>
													<div class="album">
														<a href="#"> <img alt=""
															src="../images/gallery/image1.jpg">
														</a> <a href="#"> <img alt=""
															src="../images/gallery/image2.jpg">
														</a> <a href="#"> <img alt=""
															src="../images/gallery/image3.jpg">
														</a>
													</div>
												</div>
											</li>
											<li>
												<div class="avatar">
													<img src="../images/photos/user2.png" alt="" />
												</div>
												<div class="activity-desk">
													<h5>
														<a href="#">John Doe</a> <span>Completed the Sight
															visit.</span>
													</h5>
													<p class="text-muted">2 minutes ago near Alaska, USA</p>
													<div class="location-map">
														<div id="map-canvas"></div>
													</div>
												</div>
											</li>

											<li>
												<div class="avatar">
													<img src="../images/photos/user3.png" alt="" />
												</div>
												<div class="activity-desk">

													<h5>
														<a href="#">Jonathan Smith</a> <span>attended a
															meeting with</span> <a href="#">John Doe.</a>
													</h5>
													<p class="text-muted">2 days ago near Alaska, USA</p>
												</div>
											</li>

											<li>
												<div class="avatar">
													<img src="../images/photos/user4.png" alt="" />
												</div>
												<div class="activity-desk">

													<h5>
														<a href="#">Jonathan Smith</a> <span>completed the
															task âwireframe designâ within the dead line</span>
													</h5>
													<p class="text-muted">4 days ago near Alaska, USA</p>
												</div>
											</li>

											<li>
												<div class="avatar">
													<img src="../images/photos/user5.png" alt="" />
												</div>
												<div class="activity-desk">

													<h5>
														<a href="#">Jonathan Smith</a> <span>was absent
															office due to sickness</span>
													</h5>
													<p class="text-muted">4 days ago near Alaska, USA</p>
												</div>
											</li>


										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>

			<!--footer section start-->
			<footer class="sticky-footer"> &copy; Developed by Rabeya
				Bashri, Prianti Chowdhury, Arpita Biswas </footer>
			<!--footer section end-->


		</div>
		<!-- main content end-->
	</section>

	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="../js/jquery-1.10.2.min.js"></script>
	<script src="../js/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="../js/jquery-migrate-1.2.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/modernizr.min.js"></script>
	<script src="../js/jquery.nicescroll.js"></script>


	<!--Sparkline Chart-->
	<script src="../js/sparkline/jquery.sparkline.js"></script>
	<script src="../js/sparkline/sparkline-init.js"></script>

	<!--google map-->
	<script
		src="https://maps.googleapis.com/maps/api/js?v=3.exp&AMP;sensor=false"></script>


	<!--common scripts for all pages-->
	<script src="../js/scripts.js"></script>

	<script>
		//google map
		function initialize() {
			var myLatlng = new google.maps.LatLng(-37.815207, 144.963937);
			var mapOptions = {
				zoom : 15,
				scrollwheel : false,
				center : myLatlng,
				mapTypeId : google.maps.MapTypeId.ROADMAP
			}
			var map = new google.maps.Map(
					document.getElementById('map-canvas'), mapOptions);
			var marker = new google.maps.Marker({
				position : myLatlng,
				map : map,
				title : 'Hello World!'
			});
		}
		google.maps.event.addDomListener(window, 'load', initialize);
	</script>

</body>
</html>