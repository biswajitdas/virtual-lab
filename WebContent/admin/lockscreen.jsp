<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.vls.dao.UserManagement,com.vls.beans.User"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="#" type="../image/png">

<title>Lock Screen</title>

<link href="../css/style.css" rel="stylesheet">
<link href="../css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
</head>

<jsp:useBean id="user" class="com.vls.beans.User"></jsp:useBean>
<jsp:setProperty property="*" name="user" />

<%
	int ttempid = -1;

	if (session.getAttribute("adminid") == null && session.getAttribute("ttempid") == null) {
		response.sendRedirect("index.jsp");
	} else if (session.getAttribute("ttempid") != null) {
		ttempid = Integer.parseInt(session.getAttribute("ttempid").toString());
	} else {
		ttempid = Integer.parseInt(session.getAttribute("adminid").toString());
		
		session.setAttribute("ttempid", ttempid);
	}

	session.removeAttribute("adminid");
	
	int x = 0;
	if (request.getParameter("submit") != null) {
		x = UserManagement.signin(user, 3);

		if (x != 0) {
			session.setAttribute("adminid", x);
			session.removeAttribute("ttempid");
			response.sendRedirect("profile.jsp");
		}
		request.setAttribute("x", x);
	}

	User u = UserManagement.getUser(ttempid);

	request.setAttribute("u", u);
%>

<body class="lock-screen">

	<div class="lock-wrapper">

		<div class="panel lock-box text-center">
			<img alt="lock avatar" src="../temp/GetImage?id=${u.getId()}" width="120"
				height="120">
			<div class="locked">
				<i class="fa fa-lock"></i>
			</div>
			<h1>${u.getName()}</h1>
			<div class="row">
				<c:if test="${x==0}">
					<div class="alert alert-block alert-danger fade in">
						<button type="button" class="close close-sm" data-dismiss="alert">
							<i class="fa fa-times"></i>
						</button>
						<strong>Oh snap!</strong> Wrong username or password
					</div>
				</c:if>
				<form action="lockscreen.jsp" class="form-inline" role="form"
					method="post">
					<div class="form-group col-md-12 col-sm-12 col-xs-12">
						<input type="hidden" value="${u.getUsername()}" name="username" />
						<input type="password" name="password"
							class="form-control lock-input" placeholder="Password">
						<button type="submit" name="submit" value="submit"
							class="btn btn-lock pull-right">
							<i class="fa fa-check"></i>
						</button>
					</div>
				</form>
				
			</div>
					<div class="registration">
			Sign in with different account <a href="index.jsp" class=""> Here </a>
		</div>
		</div>

	</div>

	<!-- Placed js at the end of the document so the pages load faster -->

	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="../js/jquery-1.10.2.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/modernizr.min.js"></script>

</body>
</html>
