<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.vls.dao.*,com.vls.beans.*,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="ThemeBucket">
<link rel="shortcut icon" href="#" type="../image/png">

<title>Chat With Groupmembers and Teachers</title>

<link href="../css/style.css" rel="stylesheet">
<link href="../css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="../js/html5shiv.js"></script>
  <script src="../js/respond.min.js"></script>
  <![endif]-->
</head>

<jsp:useBean id="message" class="com.vls.beans.Message"></jsp:useBean>
<jsp:setProperty property="*" name="message" />


<%
	int groupid = -1;
	int memberid = 0;
	List<Group> groups = null;
	User user = new User();

	if (session.getAttribute("teacherid") == null) {
		response.sendRedirect("index.jsp");
	} else {
		memberid = Integer.parseInt(session.getAttribute("teacherid").toString());
		//groupid = GroupManagement.getUserGroupId(memberid);

		if (request.getParameter("gid") != null)
			groupid = Integer.parseInt(request.getParameter("gid"));

		groups = GroupManagement.getAllGroupOfSupervisor(memberid);
		request.setAttribute("groups", groups);

		user = UserManagement.getUser(memberid);
		request.setAttribute("user", user);

	}

	if (request.getParameter("submit") != null) {
		int x = ChatManagement.postMessage(message);

		if (x != 0) {
			response.sendRedirect("chat.jsp?success=1&gid="+groupid);
		} else {
			response.sendRedirect("chat.jsp?error=1");
		}

	}
%>

<body class="sticky-header">

	<section>
		<!-- left side start-->
		<div class="left-side sticky-left-side">

			<!--logo and iconic logo start-->
			<div class="logo">
				<a href="index.html"><img src="../images/logo2.png" alt=""></a>
			</div>

			<div class="logo-icon text-center">
				<a href="index.html"><img src="../images/logo_icon.png" alt=""></a>
			</div>
			<!--logo and iconic logo end-->


			<div class="left-side-inner">

				<!-- visible to small devices only -->

				<!-- 
            <div class="visible-xs hidden-sm hidden-md hidden-lg">
                <div class="media logged-user">
                    <img alt="" src="images/photos/user-avatar.png" class="media-object">
                    <div class="media-body">
                        <h4><a href="#">John Doe</a></h4>
                        <span>"Hello There..."</span>
                    </div>
                </div>

                <h5 class="left-nav-title">Account Information</h5>
                <ul class="nav nav-pills nav-stacked custom-nav">
                    <li><a href="#"><i class="fa fa-user"></i> <span>Profile</span></a></li>
                    <li><a href="#"><i class="fa fa-cog"></i> <span>Settings</span></a></li>
                    <li><a href="#"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>
                </ul>
            </div>
-->

				<!--sidebar nav start-->
				<ul class="nav nav-pills nav-stacked custom-nav">

					<li><a href="profile.jsp"><i class="fa fa-bullhorn"></i> <span>Profile</span></a></li>
					<li><a href="editprofile.jsp"><i class="fa fa-bullhorn"></i> <span>Edit Profile</span></a></li>
					<li><a href="creategroup.jsp"><i class="fa fa-bullhorn"></i><span>Create Group</span></a></li>
					<li><a href="chat.jsp"><i class="fa fa-envelope"></i><span>Chat</span></a></li>
					<li><a href="showprojects.jsp"><i class="fa fa-laptop"></i><span>Supervise Projects</span></a></li>



					<!--multi level menu start-->
					<!--<li class="menu-list">-->
					<!--<a href="#"><i class="fa fa-map-marker"></i> <span>Multilavel</span></a>-->
					<!--<ul class="sub-menu-list">-->
					<!--<li class="menu-list"><a href="#"> menu1</a>-->
					<!--<ul class="sub-menu-list">-->
					<!--<li class="menu-list"><a href="#"><i class="fa fa-map-marker"></i> <span>menu2</span></a>-->
					<!--<ul class="sub-menu-list">-->
					<!--<li><a href="#"> menu2 sub</a></li>-->
					<!--<li><a href="#"> menu2 sub2</a></li>-->
					<!--</ul>-->
					<!--</li>-->
					<!--</ul>-->
					<!--</li>-->
					<!--</ul>-->
					<!--</li>-->
					<!--multi level menu end-->

				</ul>
				<!--sidebar nav end-->

			</div>
		</div>
		<!-- left side end-->

		<!-- main content start-->
		<div class="main-content">

			<!-- header section start-->
			<div class="header-section">

				<!--toggle button start-->
				<a class="toggle-btn"><i class="fa fa-bars"></i></a>
				<!--toggle button end-->

				<!--search start-->
				<!--
        <form class="searchform" action="index.html" method="post">
            <input type="text" class="form-control" name="keyword" placeholder="Search here..." />
        </form>-->
				<!--search end-->

				<!--notification menu start -->
				<div class="menu-right">
					<ul class="notification-menu">
						<!--
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                        <i class="fa fa-tasks"></i>
                        <span class="badge">8</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-head pull-right">
                        <h5 class="title">You have 8 pending task</h5>
                        <ul class="dropdown-list user-list">
                            <li class="new">
                                <a href="#">
                                    <div class="task-info">
                                        <div>Database update</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar" class="progress-bar progress-bar-warning">
                                            <span class="">40%</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="new">
                                <a href="#">
                                    <div class="task-info">
                                        <div>Dashboard done</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div style="width: 90%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="90" role="progressbar" class="progress-bar progress-bar-success">
                                            <span class="">90%</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="task-info">
                                        <div>Web Development</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div style="width: 66%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="66" role="progressbar" class="progress-bar progress-bar-info">
                                            <span class="">66% </span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="task-info">
                                        <div>Mobile App</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div style="width: 33%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="33" role="progressbar" class="progress-bar progress-bar-danger">
                                            <span class="">33% </span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="task-info">
                                        <div>Issues fixed</div>
                                    </div>
                                    <div class="progress progress-striped">
                                        <div style="width: 80%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="80" role="progressbar" class="progress-bar">
                                            <span class="">80% </span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="new"><a href="">See All Pending Task</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                        <i class="fa fa-envelope"></i>
                        <span class="badge">5</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-head pull-right">
                        <h5 class="title">You have 5 Mails </h5>
                        <ul class="dropdown-list normal-list">
                            <li class="new">
                                <a href="">
                                    <span class="thumb"><img src="images/photos/user1.png" alt="" /></span>
                                        <span class="desc">
                                          <span class="name">John Doe <span class="badge badge-success">new</span></span>
                                          <span class="msg">Lorem ipsum dolor sit amet...</span>
                                        </span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="thumb"><img src="images/photos/user2.png" alt="" /></span>
                                        <span class="desc">
                                          <span class="name">Jonathan Smith</span>
                                          <span class="msg">Lorem ipsum dolor sit amet...</span>
                                        </span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="thumb"><img src="images/photos/user3.png" alt="" /></span>
                                        <span class="desc">
                                          <span class="name">Jane Doe</span>
                                          <span class="msg">Lorem ipsum dolor sit amet...</span>
                                        </span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="thumb"><img src="images/photos/user4.png" alt="" /></span>
                                        <span class="desc">
                                          <span class="name">Mark Henry</span>
                                          <span class="msg">Lorem ipsum dolor sit amet...</span>
                                        </span>
                                </a>
                            </li>
                            <li>
                                <a href="">
                                    <span class="thumb"><img src="images/photos/user5.png" alt="" /></span>
                                        <span class="desc">
                                          <span class="name">Jim Doe</span>
                                          <span class="msg">Lorem ipsum dolor sit amet...</span>
                                        </span>
                                </a>
                            </li>
                            <li class="new"><a href="">Read All Mails</a></li>
                        </ul>
                    </div>
                </li>
                <li>
                    <a href="#" class="btn btn-default dropdown-toggle info-number" data-toggle="dropdown">
                        <i class="fa fa-bell-o"></i>
                        <span class="badge">4</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-head pull-right">
                        <h5 class="title">Notifications</h5>
                        <ul class="dropdown-list normal-list">
                            <li class="new">
                                <a href="">
                                    <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                    <span class="name">Server #1 overloaded.  </span>
                                    <em class="small">34 mins</em>
                                </a>
                            </li>
                            <li class="new">
                                <a href="">
                                    <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                    <span class="name">Server #3 overloaded.  </span>
                                    <em class="small">1 hrs</em>
                                </a>
                            </li>
                            <li class="new">
                                <a href="">
                                    <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                    <span class="name">Server #5 overloaded.  </span>
                                    <em class="small">4 hrs</em>
                                </a>
                            </li>
                            <li class="new">
                                <a href="">
                                    <span class="label label-danger"><i class="fa fa-bolt"></i></span>
                                    <span class="name">Server #31 overloaded.  </span>
                                    <em class="small">4 hrs</em>
                                </a>
                            </li>
                            <li class="new"><a href="">See All Notifications</a></li>
                        </ul>
                    </div>
                </li>
               -->
						<li><a href="#" class="btn btn-default dropdown-toggle"
							data-toggle="dropdown"> <img
								src="../temp/GetImage?id=${user.getId()}" alt="" />
								${user.getName() } <span class="caret"></span>
						</a>
							<ul class="dropdown-menu dropdown-menu-usermenu pull-right">
								<li><a href="profile.jsp"><i class="fa fa-user"></i>
										Profile</a></li>
								<li><a href="editprofile.jsp"><i class="fa fa-cog"></i>
										Edit Profile</a></li>
								<li><a href="lockscreen.jsp"><i
										class="fa fa-sign-out-alt"></i> Log Out</a></li>
							</ul></li>

					</ul>
				</div>
				<!--notification menu end -->

			</div>
			<!-- header section end-->

			<!-- page heading start-->
			<div class="page-heading">
				<h3>Chat with supervisor and group member</h3>
				<!--ul class="breadcrumb">
                <li>
                    <a href="#">Extra Pages</a>
                </li>
                <li class="active"> Chat </li>
            </ul-->
			</div>
			<!-- page heading end-->


			<!--body wrapper start-->
			<div class="wrapper">
				<div class="row">

					<div class="col-md-6">
						<div class="panel">
							<header class="panel-heading">
								Chat with group members <span class="tools pull-right"> <a
									href="javascript:;" class="fa fa-chevron-down"></a> <a
									href="javascript:;" class="fa fa-times"></a>
								</span>
							</header>
							<div class="panel-body">
								<ul class="chats normal-chat"
									style="height: 450px; overflow: auto;" id="chathistory1">


								</ul>
								<div class="chat-form ">
									<form role="form" class="form-inline" method="post">
										<div class="form-group">
											<input type="hidden" value="<%=memberid%>" name="userid" />
											<input type="hidden" value="<%=groupid%>" name="groupid" />
											<input type="hidden" value="1" name="withteacher" /> <select
												class="form-control m-bot15" name="groupid" id="gid"
												onchange="getChatMessages()">
												<option value="">Select Group</option>
												<c:forEach items="${groups}" var="g">
													<option value="${g.getId()}"
														${param.gid==g.getId()?"selected":"" }>${g.getGroupname()}</option>
												</c:forEach>
											</select>

										</div>
										<div class="form-group">
											<input type="text" style="width: 100%" name="message"
												placeholder="Type a message here..." class="form-control">

										</div>
										<button class="btn btn-primary" name="submit" value="submit"
											type="submit">Send</button>
									</form>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-6">
						<div class="panel">
							<header class="panel-heading">
								Chat with Teachers <span class="tools pull-right"> <a
									href="javascript:;" class="fa fa-chevron-down"></a> <a
									href="javascript:;" class="fa fa-times"></a>
								</span>
							</header>
							<div class="panel-body">
								<ul class="chats cool-chat"
									style="height: 450px; overflow: auto;" id="chathistory2">




								</ul>

								<div class="chat-form ">
									<form role="form" class="form-inline" method="post">
										<div class="form-group">
											<input type="hidden" value="<%=memberid%>" name="userid" />
											<input type="hidden" value="0" name="groupid" />
											<input type="hidden" value="1" name="withteacher" /> <input
												type="text" style="width: 100%" name="message"
												placeholder="Type a message here..." class="form-control">
										</div>
										<button class="btn btn-primary" name="submit" value="submit"
											type="submit">Send</button>
									</form>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
			<!--body wrapper end-->

			<!--footer section start-->
			<footer class="sticky-footer"> &copy; Developed by Rabeya
				Bashri, Prianti Chowdhury, Arpita Biswas </footer>
			<!--footer section end-->


		</div>
		<!-- main content end-->
	</section>

	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="../js/jquery-1.10.2.min.js"></script>
	<script src="../js/jquery-ui-1.9.2.custom.min.js"></script>
	<script src="../js/jquery-migrate-1.2.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/modernizr.min.js"></script>
	<script src="../js/jquery.nicescroll.js"></script>


	<!--common scripts for all pages-->
	<script src="../js/scripts.js"></script>


	<script>
    setInterval(loadDoc1, 500);

	loadDoc1();
	
    function loadDoc1() {


            var textarea = document.getElementById("chathistory1");

            textarea.style.overflow = "auto";

            textarea.scrollTop = textarea.scrollHeight-200;

        var xhttp;
        if (window.XMLHttpRequest) {
            // code for modern browsers
            xhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("chathistory1").innerHTML = this.responseText;
            }
        };
        xhttp.open("GET", "chatretrieve.jsp?groupid=<%=groupid%>&memberid=<%=memberid%>&withteacher=1",true);
			xhttp.send();

		}
    
    
    
    setInterval(loadDoc2, 500);

	loadDoc2();
	
    function loadDoc2() {


            var textarea = document.getElementById("chathistory2");

            textarea.style.overflow = "auto";

            textarea.scrollTop = textarea.scrollHeight-200;

        var xhttp;
        if (window.XMLHttpRequest) {
            // code for modern browsers
            xhttp = new XMLHttpRequest();
        } else {
            // code for IE6, IE5
            xhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("chathistory2").innerHTML = this.responseText;
            }
        };
        xhttp.open("POST", "chatretrieve.jsp?groupid=0&memberid=<%=memberid%>&withteacher=1",true);
		xhttp.send();

		
		
		
		}
	</script>

	<script type="text/javascript">
		function getChatMessages() {
			var gid = document.getElementById('gid').value;

			//var cid = document.getElementById('cid').value;
			if (gid) {
				window.location = 'chat.jsp?gid=' + gid;
			} else {
				window.location = 'chat.jsp';
			}
		}
	</script>


</body>
</html>
