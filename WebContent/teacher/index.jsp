<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.vls.dao.UserManagement,com.vls.beans.User"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="#" type="../image/png">

<title>Sign In</title>

<link href="../css/style.css" rel="stylesheet">
<link href="../css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
</head>

<jsp:useBean id="user" class="com.vls.beans.User"></jsp:useBean>
<jsp:setProperty property="*" name="user" />

<%
	if (session.getAttribute("teacherid") != null) {
		response.sendRedirect("profile.jsp");
	}

	session.removeAttribute("ttempid");

	int x = 0;
	if (request.getParameter("signup") != null) {
		x = UserManagement.signin(user, 2);

		if (x != 0) {
			session.setAttribute("teacherid", x);

			response.sendRedirect("profile.jsp");
		}
		request.setAttribute("x", x);
	}
%>

<body class="login-body">

	<div class="container">

		<form class="form-signin" action="index.jsp" method="post">
			<div class="form-signin-heading text-center">
				<h1 class="sign-title">Teacher Panel | Sign In</h1>
				<!--img src="images/login-logo.png" alt=""/-->
			</div>
			<div class="login-wrap">

				<c:if test="${param.success==1}">
					<div class="alert alert-success alert-block fade in">
						<button type="button" class="close close-sm" data-dismiss="alert">
							<i class="fa fa-times"></i>
						</button>
						<h4>
							<i class="icon-ok-sign"></i> Success!
						</h4>
						<p>Registration Successful, Login Please</p>
					</div>
				</c:if>

				<c:if test="${x==0}">
					<div class="alert alert-block alert-danger fade in">
						<button type="button" class="close close-sm" data-dismiss="alert">
							<i class="fa fa-times"></i>
						</button>
						<strong>Oh snap!</strong> Wrong username or password
					</div>
				</c:if>

				<input type="text" class="form-control" name="username"
					placeholder="User ID" autofocus required> <input type="password"
					class="form-control" name="password" placeholder="Password" required>

				<button class="btn btn-lg btn-login btn-block" type="submit"
					value="submit" name="signup">
					<i class="fa fa-check"></i>
				</button>

				<div class="registration">
					Not a member yet? <a class="" href="signup.jsp"> Signup </a>
				</div>
				<!--
			
            <label class="checkbox">
                <input type="checkbox" value="remember-me"> Remember me
                <span class="pull-right">
                    <a data-toggle="modal" href="#myModal"> Forgot Password?</a>

                </span>
            </label>
			
			-->

			</div>


		</form>

	</div>



	<!-- Placed js at the end of the document so the pages load faster -->

	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="../js/jquery-1.10.2.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/modernizr.min.js"></script>

</body>
</html>
