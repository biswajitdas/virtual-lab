<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="com.vls.dao.*,com.vls.beans.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="#" type="../image/png">

<title>Registration</title>

<!--ios7-->
<link rel="stylesheet" type="text/css"
	href="../js/ios-switch/switchery.css" />


<!--multi-select-->
<link rel="stylesheet" type="text/css"
	href="../js/jquery-multi-select/css/multi-select.css" />

<!--file upload-->
<link rel="stylesheet" type="text/css"
	href="../css/bootstrap-fileupload.min.css" />

<!--tags input-->
<link rel="stylesheet" type="text/css"
	href="../js/jquery-tags-input/jquery.tagsinput.css" />


<link href="../css/style.css" rel="stylesheet">
<link href="../css/style-responsive.css" rel="stylesheet">

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="../js/html5shiv.js"></script>
    <script src="../js/respond.min.js"></script>
    <![endif]-->
</head>

<jsp:useBean id="user" class="com.vls.beans.User"></jsp:useBean>
<jsp:setProperty property="*" name="user" />

<%
	if (request.getParameter("submit") != null) {
		int x = UserManagement.registerUser(user);

		if (x == 1) {
			response.sendRedirect("index.jsp?success=1");
		}

	}
%>

<body class="login-body">

	<div class="container">

		<form class="form-signin" action="uploadServlet" method="post"
			enctype="multipart/form-data">
			<div class="form-signin-heading text-center">
				<h1 class="sign-title">Teacher Sign up</h1>
				<!--img src="images/login-logo.png" alt=""/-->
			</div>

			<div class="login-wrap">

				<c:if test="${x==2}">
					<div class="alert alert-block alert-danger fade in">
						<button type="button" class="close close-sm" data-dismiss="alert">
							<i class="fa fa-times"></i>
						</button>
						<strong>Oh snap!</strong> Change a few things up and try
						submitting again.
					</div>
				</c:if>

				<input type="hidden" name="id" value="0" /> <input type="hidden"
					name="type" value="2" /> <input type="hidden" name="status"
					value="Status not set" />


				<p>Enter your personal details below</p>
				<input type="text" autofocus="autofocus" placeholder="Full Name"
					name="name" class="form-control" required="required"> <input
					type="text" placeholder="Address" name="address"
					class="form-control" required="required"> <input
					type="email" placeholder="Email" name="email" class="form-control"
					required="required"> <select name="department"
					class="form-control input-sm m-bot15" required="required">
					<option value="">Select Department</option>
					<option value="CSE">CSE</option>
					<option value="EEE">EEE</option>
				</select> <select class="form-control input-sm m-bot15"
					name="allowedprojects" required="required">
					<option value="">Select Allowed Projects</option>
					<c:forEach begin="1" end="10" var="i">
						<option value="${i}">${i}</option>
					</c:forEach>
				</select> <select class="form-control input-sm m-bot15" name="des"
					required="required">
					<option value="">Select Designation</option>
					<option value="2">Teaching Assistant</option>
					<option value="3">Lecturer</option>
					<option value="4">Assistant Professor</option>
					<option value="5">Associate Professor</option>
					<option value="6">Professor</option>
				</select>

				<div class="radios">
					<label for="radio-01" class="label_radio col-lg-6 col-sm-6">
						<input type="radio" value="1" id="radio-01" name="gender"
						required="required"> Male
					</label> <label for="radio-02" class="label_radio col-lg-6 col-sm-6">
						<input type="radio" value="2" id="radio-02" name="gender"
						required="required"> Female
					</label>
				</div>
				<div class="col-lg-12 col-sm-12">
					<div class="fileupload fileupload-new" data-provides="fileupload">
						<input value="" name="" type="hidden">
						<div class="fileupload-new thumbnail"
							style="width: 200px; height: 150px;">
							<img
								src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image"
								alt="">
						</div>
						<div class="fileupload-preview fileupload-exists thumbnail"
							style="max-width: 200px; max-height: 150px; line-height: 10px;"></div>
						<div>
							<span class="btn btn-default btn-file"> <span
								class="fileupload-new"><i class="fa fa-paper-clip"></i>
									Select image</span> <span class="fileupload-exists"><i
									class="fa fa-undo"></i> Change</span> <input class="default"
								name="image" type="file">
							</span> <a href="#" class="btn btn-danger fileupload-exists"
								data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
						</div>
					</div>
					<!--
                                        <br>
                                        <span class="label label-danger ">NOTE!</span>
                                             <span>
                                             Attached image thumbnail is
                                             supported in Latest Firefox, Chrome, Opera,
                                             Safari and Internet Explorer 10 only
                                             </span>
											 -->
				</div>


				<p>Enter your account details below</p>
				<input type="text" placeholder="User Name" name="username"
					class="form-control" required="required"> <input
					type="password" placeholder="Password" name="password"
					class="form-control" required="required">
				<button type="submit" value="submit" name="submit"
					class="btn btn-lg btn-login btn-block">
					<i class="fa fa-check"></i>
				</button>

				<div class="registration">
					Already Registered. <a href="index.jsp" class=""> Login </a>
				</div>

			</div>

		</form>

	</div>


	<!-- Placed js at the end of the document so the pages load faster -->
	<script src="../js/jquery-1.10.2.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/modernizr.min.js"></script>

	<!--file upload-->
	<script type="text/javascript" src="../js/bootstrap-fileupload.min.js"></script>

</body>
</html>
