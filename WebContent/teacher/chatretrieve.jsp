<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="com.vls.beans.Message,com.vls.dao.ChatManagement,java.util.*"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%
	int groupid = Integer.parseInt(request.getParameter("groupid"));
	int memberid = Integer.parseInt(request.getParameter("memberid"));
	int withteacher = Integer.parseInt(request.getParameter("withteacher"));

	List<Message> list = ChatManagement.getMessage(groupid, withteacher);

	request.setAttribute("list", list);
%>

<c:forEach items="${list}" var="m">

	<c:set var="x" value="${m.getUserid()}"></c:set>

	<%
		int x = Integer.parseInt(pageContext.getAttribute("x").toString());
	%>

	<li class="<%=(x == memberid) ? "out" : "in"%>"><img
		src="../temp/GetImage?id=${m.getUserid()}" alt="" class="avatar">
		<div class="message">
			<span class="arrow"></span> <a class="name" href="#">${m.getName()}</a>
			<span class="datetime">at ${m.getDateString()}</span> <span
				class="body"> ${m.getMessage()} </span>
		</div></li>

</c:forEach>
